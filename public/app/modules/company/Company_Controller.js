$APP.routes['company'] = {
    url:'/company',
    template: '<div ui-view ></div>'
};
/**
|   SEARCH COMPANY   
**/
$APP.routes['company.search'] = {
	url: '/search',
    templateUrl: $APP.view('company/views/search'),
    data:{
        pageTitle:'Search company'
    },
    controller: function( $scope, pagination, $http ){
		
        $scope.filters = {
            name:'',
            address:'',
            contact_name:'',
            contact_phone:'',
            status:'ALL'
    	};

        $http.get($APP.server('company/search-initial/')).success( function( response ){
            $scope.status_company = response.status_company; 
        });

    	$scope.pagination = new pagination('company/search', $scope.filters );
		$scope.pagination.getData(1);
    }
};
/**
|   CREATE COMPANY   
**/
$APP.routes['company.create'] = {
    url: '/create',
    templateUrl: $APP.view('company/views/create'),
    data:{
        pageTitle:'Create company'
    },
    controller: function( $scope , $http, $state , $uibModal ){
        
        $scope.formDisabled = false;
        
        $scope.company = {
            user_owner:0,
            name:'',
            address:'',
            contact_name:'Unknowledge',
            contact_phone:'',
            contact_email:''
        };
        
        $http.get($APP.server('user-available/all/')).success( function( response ){
            $scope.company.user_owner = angular.copy( $state.current_user.id );
            $scope.user_list          = response.user_list;
        });

        $scope.form_submit_create_company = function(){
            
            $scope.formDisabled       = true;
            
            $http.post( $APP.server('company/create/'), $scope.company ).success(function( response ){
                if(!response.status )
                {
                    toastr.error( response.message );
                    $scope.formDisabled = false;
                }
                else
                {   
                    toastr.success( response.message );
                    $state.go('company.edit', { 'id' : response.company.id } );
                }

               
            });
        }

    }
};
/**
|   EDIT COMPANY   
**/
$APP.routes['company.edit'] = {
    url: '/:id/edit',
    templateUrl: $APP.view('company/views/edit'),
    data:{
        pageTitle:'Edit company'
    },
    controller: function( $scope , $http, $stateParams , $uibModal, SweetAlert, $state  ) {
        
        $state.company_name = ' ... ';

        $http.get($APP.server('company/'+$stateParams.id+'/info/')).success( function( response ){
            
            $scope.user_list             = response.user_list;
            $state.company_name          = response.company.name;
           
            $scope.company               = response.company;
            $scope.contact_list          = response.contact_list;
            $scope.communications        = response.communications;
            $scope.types_communication   = response.types_communication;
            $scope.status_company        = response.status_company;

        });
        
        $scope.formDisabled = false;
        
        $scope.form_submit_update_company = function() {
            
            $scope.formDisabled = true;
            
            $http.post( $APP.server('company/'+$scope.company.id+'/update/'), $scope.company ).success(function(response){
                
                if(!response.status )
                {
                    toastr.error( response.message );
                }
                else
                {
                    toastr.success( response.message );
                }

                $scope.formDisabled = false;
                
            });
        }

        /**
        |------------------------
        | Contact List Actions
        |------------------------
        **/
        var action_contact_list = function(){
            
           
            var $self   = this;

            this.open_form = function( contact ){
                
                var $parent = $scope;

                $uibModal.open({
                    templateUrl: $APP.view('company/views/modal-contact-list'),
                    size: 'lg',
                    controller: function( $scope, $uibModalInstance, $http  ){
                        if(typeof contact === 'undefined')
                        {
                            $self.reset_form( $scope );
                        }
                        else
                        {
                            $scope.contact = angular.copy(contact);
                        }

                        $scope.save = function()
                        {
                            $scope.formDisabled = true;
                            
                            $http.post( $APP.server('company/contact-list/'+$parent.company.id+'/save/'), $scope.contact ).success(function(response){
                                
                                if(!response.status )
                                {

                                    toastr.error( response.message );
                                }
                                else
                                {   
                                    toastr.success( response.message );
                                    $parent.contact_list = response.contact_list;
                                    $uibModalInstance.close();
                                }

                                $scope.formDisabled = false;
                                
                            });
                        }
                    }
                });
            };

            this.reset_form = function(){
                $scope.contact = {
                    id:0,
                    names:'',
                    phone:'',
                    email:'',
                    default:0,
                };
            };

            this.remove = function( contact ){
                SweetAlert.swal({
                    title: "Are you sure you want to remove contact?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    //cancelButtonText: 'Cancel',
                    confirmButtonColor: "#DD6B55",
                    //confirmButtonText: "Yes, Confirm remove!",
                    //closeOnConfirm: true
                }, function ( isConfirm ) {

                    if(isConfirm)
                    {   
                        $http.delete($APP.server('company/contact-list/'+contact.id+'/delete/'));
                        //swal("Cita eliminada!", "La Cita fue eliminada.", "success");
                        
                        $scope.contact_list.splice( $scope.contact_list.indexOf(contact) , 1 );
                    }
                });
            };
        };

        /**
        |------------------------
        | Communications
        |------------------------
        **/
        var action_communication = function(){
            
            var $self   = this;

            this.open_form = function( communication ){
                
                var $parent = $scope;

                $uibModal.open({
                    templateUrl: $APP.view('company/views/modal-communication'),
                    size: 'lg',
                    controller: function( $scope, $uibModalInstance, $http  ){
                        $scope.save = function(){
                            $scope.formDisabled = true;
                            
                            $http.post( $APP.server('company/communication/'+$parent.company.id+'/save/'), $scope.communication ).success(function(response){
                                
                                if(!response.status )
                                {
                                    toastr.error( response.message );
                                }
                                else
                                {   
                                    toastr.success( response.message );
                                    $parent.communications.unshift(response.communication);
                                    $uibModalInstance.close();
                                }

                                $scope.formDisabled = false;
                                
                            });
                        }

                        $scope.reset_form = function(){
                            $scope.communication = {
                                id:0,
                                type:"1",
                                message:''
                            };
                        } 
                        
                        if(typeof communication === 'undefined')
                        {
                            $scope.reset_form();
                        }
                        else
                        {
                            $scope.communication = communication;
                        }
                    }
                });
            };
        };

        $scope.action_contact_list = new action_contact_list(); 
        $scope.action_communication = new action_communication();

    }
};
/**
|   COMPANY SETTINGS
**/
$APP.routes['company.settings'] = {
    url: '/settings',
    template: '****Settings****',
    data:{
        pageTitle:'Company Settings'
    },
    //templateUrl: $APP.view('company/views/settings'),
    controller: function( $scope , $http, $stateParams ) {
        console.log("Settings Company")
    }
};

