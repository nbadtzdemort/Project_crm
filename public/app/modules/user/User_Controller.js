$APP.routes['users'] = {
	url: '/users',
    templateUrl: $APP.view('user/views/search'),
    controller: function( $scope, pagination ){
		$scope.filters = {
    		email:'',
    		names:'',
            role:'ALL'
    	}
    	$scope.pagination = new pagination('user/search', $scope.filters );
		$scope.pagination.getData(1);
    }
};

$APP.routes['userCreate'] = {
    url: '/users/create',
    templateUrl: $APP.view('user/views/create'),
    controller: function($scope, $http , $state ) {
        
        $scope.user = {
            email:'',
            names:'',
            role:'collaborator',
            password:''
        };
        
        $scope.formDisabled = false;

        $scope.form_submit_create_user = function() {
            $scope.formDisabled = true;
            
            $http.post( $APP.server('user/create/'), $scope.user ).success(function( response ){
                if(!response.status )
                {
                    toastr.error( response.message );
                    $scope.formDisabled = false;
                }
                else
                {   
                    toastr.success( response.message );
                    $state.go('userEdit', { 'id' : response.user.id } );
                }

               
            })

            

        }
    }
};

$APP.routes['userEdit'] = { 
    url: '/users/:id/edit',
    templateUrl: $APP.view('user/views/edit'),
    controller: function( $scope , $http, $stateParams ) {
        
        $http.get($APP.server('user/'+$stateParams.id+'/info/')).success( function( response ){
            $scope.user = response.user;
        });
        
        $scope.formDisabled = false;
        
        $scope.form_submit_update_user = function() {
            
            $scope.formDisabled = true;
            
            $http.post( $APP.server('user/'+$scope.user.id+'/update/'), $scope.user ).success(function(response){
               
                
                if(!response.status )
                {
                    toastr.error( response.message );
                }
                else
                {
                    toastr.success( response.message );
                }

                $scope.formDisabled = false;
                
            });
            

        }
    }
};

$APP.routes['profile'] = {
    url: '/profile',
    templateUrl: $APP.view('user/views/profile'),
    controller: function( $scope , $http ) {
        
        $http.get($APP.server('user/profile/info/')).success( function( response ){
            $scope.user = response.user;
        });
        
        $scope.formDisabled = false;
        
        $scope.form_submit_update_profile = function() {
            
            $scope.formDisabled = true;
            
            $http.post( $APP.server('user/profile/update/'), $scope.user ).success(function(response){
                
                if(!response.status )
                {
                    toastr.error( response.message );
                }
                else
                {
                    toastr.success( response.message );
                }

                $scope.formDisabled = false;
                
            });
            
        }
    }
};