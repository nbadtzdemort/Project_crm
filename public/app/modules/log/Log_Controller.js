$APP.routes['log'] = {
    url:'/log',
    template: '<div ui-view ></div>'
}

$APP.routes['log.myLog'] = {
    url: '/myLog',
    templateUrl: $APP.view('log/views/search'),
    controller: function( $scope, pagination, $http ){
        $http.get( $APP.server('log/myLogs/')).success(function( response ){
            $scope.logs=response.logs; 
            $scope.type=response.type;
            allLogs=response.logs;
            $scope.myLogsDate = function(type,user_exist,data) {
                search = {
                    search:type,
                    user:user_exist,
                    value:data
                }
                if(data!=''){
                    $http.post( $APP.server('log/LogsSearch/'),search).success(function( response ){
                        $scope.logs=response.logs;
                    })
                }else{
                    $scope.logs=response.logs;
                }
            }
        });
    }
};

$APP.routes['log.allLog'] = {
    url: '/allLog',
    templateUrl: $APP.view('log/views/search'),
    controller: function( $scope, pagination, $http ){
        $http.get( $APP.server('log/allLogs/')).success(function( response ){
            $scope.logs=response.logs;
            $scope.type=response.type;
            allLogs=response.logs;
            $scope.myLogsDate = function(type,user_exist,data) {
                search = {
                    search:type,
                    user:user_exist,
                    value:data
                }
                if(data!=''){
                    $http.post( $APP.server('log/LogsSearch/'),search).success(function( response ){
                        $scope.logs=response.logs;
                    })
                }else{
                    $scope.logs=response.logs;
                }
            }
        });


    }
};