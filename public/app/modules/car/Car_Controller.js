$APP.routes['car'] = {
    url:'/car',
    template: '<div ui-view ></div>'
}

$APP.routes['car.list'] = {
    url: '/list',
    templateUrl: $APP.view('car/views/search'),
    controller: function( $scope, pagination ){
        
        $scope.filters = {
            name:'',
            model:''     };

        $scope.pagination = new pagination('car/search', $scope.filters );
        $scope.pagination.getData(1);
    }
};

$APP.routes['car.new_car'] = {
    url: '/create',
    templateUrl: $APP.view('car/views/create'),
    controller: function($scope, $http , $state ) {
        
        $scope.car = {
            name:'',
            model:'',     };
        
        $scope.formDisabled = false;

        $scope.form_submit_create_car = function() {
            $scope.formDisabled = true;

            $http.post( $APP.server('car/create/'), $scope.car ).success(function( response ){

                if(!response.status )
                {   
                    toastr.error( response.message );
                    $scope.formDisabled = false;

                }
                else
                {
                    toastr.success( response.message );
                    $state.go('car.edit', { 'id' : response.car.id } );
                }

               
            })

        }
    }
};

$APP.routes['car.edit'] = { 
    url: '/:id/edit',
    templateUrl: $APP.view('car/views/edit'),
    controller: function( $scope , $http, $stateParams , $uibModal, SweetAlert, $state  ) {

        $state.car_name = '';

        $http.get($APP.server('car/'+$stateParams.id+'/info/')).success( function( response ){
            $scope.car = response.car;
        });
        
        $scope.formDisabled = false;
        
        $scope.form_submit_update_car = function() {
            
            $scope.formDisabled = true;
            
            $http.post( $APP.server('car/'+$scope.car.id+'/update/'), $scope.car).success(function(response){
                
                if(!response.status )
                {
                    toastr.error( response.message );
                }
                else
                {
                    toastr.success( response.message );
                }

                $scope.formDisabled = false;
                
            });
        }

    }    
};


