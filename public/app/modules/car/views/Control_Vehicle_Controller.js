$APP.routes['car.control'] = { 
    url: '/control',
    templateUrl: $APP.view('car/views/tab-control'),
    controller: function( $scope , $http, $stateParams , $uibModal, SweetAlert, $state  ) {

        $http.get($APP.server('car/controlInfo/'))
            .success( function( response ){
                $scope.car = response.car;
                $scope.driver = response.driver;
                $scope.control = response.control;
                $scope.location = response.location;
            })
            .finally(function(){
                setTimeout(function(){ 
                $('.table').trigger('footable_redraw'); 
                }, 1);
            });

        var action_control = function(){
            
            var $self   = this;
            this.open_form = function(){
                var $parent = $scope;
                $uibModal.open({
                    templateUrl: $APP.view('car/views/control'),
                    size: 'lg',
                    controller: function( $scope, $uibModalInstance, $http  ){
                        $scope.driver=$parent.driver;
                        $scope.car=$parent.car;
                        $scope.location=$parent.location;
                        $scope.control = {};
                        $scope.control.name_car = $scope.car[$scope.car.length-1].name;
                        $scope.newControl = function()
                        { 
                           $scope.formDisabled = true; 
                            $http.post( $APP.server('car/newControl/'), $scope.control ).success(function(response){
                                
                                if(!response.status )
                                {
                                    toastr.error( response.message );
                                }
                                else
                                {   
                                    toastr.success( response.message );
                                    $parent.control.push(response.control);
                                    $uibModalInstance.close();


                                }

                                $scope.formDisabled = false;
                                
                            })
                            .finally(function(){
                                setTimeout(function(){ 
                                $('.table').trigger('footable_redraw'); 
                                }, 1);
                            });
                        }
                    }
                });
            };
            
            this.reset_form = function(){
                $scope.contact = {
                    id:0,
                    type:'1',
                    message:''
                };
            };
        };

        
        $scope.action_control = new action_control();

    }
};