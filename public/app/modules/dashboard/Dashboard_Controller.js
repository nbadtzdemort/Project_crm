$APP.routes['inicio'] = {
	url: '/',
	data: { pageTitle: 'Dashboard Application' },
    templateUrl: $APP.view('dashboard/views/index'),
    controller: function( $scope, $http ){
    	
    	$scope.total_companies = 0;
    	
    	$http.get($APP.server('application/init')).success(function(response){
    		$scope.response = response; 
    	}).finally(function(){
    		
    		for(var i=0; i< $scope.response.chart_company.data.length ; i++)
    		{
    			$scope.total_companies+= parseInt($scope.response.chart_company.data[i]);
    		}
    		console.log($scope.response.chart_company);
    		var config = {
		        type: 'pie',
		        data: {
		            datasets: [{
		                data: $scope.response.chart_company.data,
		                backgroundColor: [
							"rgb(255, 99, 132)",
							"rgb(75, 192, 192)",
							"rgb(255, 159, 64)",
							"rgb(255, 205, 86)",
							"rgb(54, 162, 235)",
		                ],
		                label: 'Dataset 1'
		            }],
		            labels: $scope.response.chart_company.labels

		        },
		        options: {
		            responsive: false
		        }
		    };

		    var ctx = document.getElementById("companies-chart").getContext('2d');
	        
	        window.myPie = new Chart(ctx, config);
    		
    	});
    	
    }
};


