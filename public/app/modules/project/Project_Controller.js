$APP.routes['project'] = {
    url:'/project',
    template: '<div ui-view ></div>'
}

$APP.routes['project.list'] = {
    url: '/list',
    templateUrl: $APP.view('project/views/search'),
    controller: function( $scope, pagination ){
        
        $scope.filters = {
            name:'',
            create_date:'',
            end_date:'', 
            description:'',
            company:''      };

        $scope.pagination = new pagination('project/search', $scope.filters );
        $scope.pagination.getData(1);
    }
};

$APP.routes['project.new'] = {
    url: '/create',
    templateUrl: $APP.view('project/views/create'),
    controller: function($scope, $http , $state ) {
        
        $scope.project = {
            name:'',
            company:'',
            end_date:'',
            description:''      };
        $scope.companys=[];
        $scope.formDisabled = false;
        $scope.autocomplete = function(company) {
            if(company!=''){
                $http.get( $APP.server('company/'+company+'/infoCompany/')).success(function( response ){
                    $scope.companys=response.company;
                })
            } 
        }

        $scope.form_submit_create_project = function() {
            $scope.formDisabled = true;

            $http.post( $APP.server('project/create/'), $scope.project ).success(function( response ){

                if(!response.status )
                {   
                    toastr.error( response.message );
                    $scope.formDisabled = false;

                }
                else
                {
                    toastr.success( response.message );
                    $state.go('project.edit', { 'id' : response.project.id } );
                }

               
            })

            

        }
    }
};

$APP.routes['project.edit'] = { 
    url: '/:id/edit',
    templateUrl: $APP.view('project/views/edit'),
    controller: function( $scope , $http, $stateParams , $uibModal, SweetAlert, $state  ) {
        $scope.companys=[];
        $state.project_name = '';
        

        $http.get($APP.server('project/'+$stateParams.id+'/info/')).success( function( response ){
            $scope.project      = response.project; 
            $scope.members      = response.member; 
            $state.project_name = response.project.name;
            $scope.id           = response.project.id;
            $scope.users        = response.user;
            $scope.tasks        = response.task;
            $scope.company      = response.company;
            $scope.companys     = $scope.company.name;
            $scope.autocomplete($scope.companys);
            $scope.project.company_name = { 
                                            name: $scope.company.name,  
                                            id: $scope.company.id
                                          };
            
            
        }).finally(function() {
            setTimeout(function(){
            if($scope.tasks.length>0){
            $scope.config_data=[];
            $scope.config_data.data=[0,0,0];
            $scope.config_data.label=['To-Do','In Progress','Complete'];
            for (var i = 0; i < $scope.tasks.length; i++) 
            {
                if($scope.tasks[i].status=="0")
                {
                    $scope.config_data.data[0]++;
                }
                else if($scope.tasks[i].status=="1")
                {
                     $scope.config_data.data[1]++;
                }
                else
                {
                     $scope.config_data.data[2]++;
                }
            }
            $scope.total=$scope.config_data.data[0]+$scope.config_data.data[1]+$scope.config_data.data[2];
            $scope.config_data.label=['To-Do '+(($scope.config_data.data[0]/$scope.total)*100).toFixed(2)+"% ",
                                      'In Progress '+(($scope.config_data.data[1]/$scope.total)*100).toFixed(2)+"% ",
                                      'Complete '+(($scope.config_data.data[2]/$scope.total)*100).toFixed(2)+"% "];
            $scope.progress=(($scope.config_data.data[2]/$scope.total)*100).toFixed(2)+"%";
            var config = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: $scope.config_data.data,
                        backgroundColor: [
                            "rgb(209, 218, 222)",
                            "rgb(26, 179, 148)",
                            "rgb(28, 132, 198)"
                        ],
                        label: 'Dataset 3'
                    }],
                    labels: $scope.config_data.label
                },
                options: {
                    responsive: true
                }
            };

            var ctx = document.getElementById("task-chart").getContext('2d');
            
            window.myPie = new Chart(ctx, config);}}, 10);
        });
        
        $scope.autocomplete = function(company) {
            if(company!=''){
                $http.get( $APP.server('company/'+company+'/infoCompany/')).success(function( response ){
                    $scope.companys=response.company;

                })
            }
        }


        $scope.formDisabled = false;
        $scope.form_submit_update_project = function() {
            
            $scope.formDisabled = true;
            
            $http.post( $APP.server('project/'+$scope.project.id+'/update/'), $scope.project ).success(function(response){
                
                if(!response.status )
                {
                    toastr.error( response.message );
                }
                else
                {
                    toastr.success( response.message );
                }

                $scope.formDisabled = false;
                
            });
        }

        var action_project_list = function(){
            var $self   = this;
            this.add_Member = function(){
               
                $scope.member={
                    user_id:$scope.project.user_id,
                    project_id:$scope.project.id
                };

                $http.post( $APP.server('member/'+$scope.project.id+'/add/'), $scope.member ).success(function(response){
                
                if(!response.status )
                {
                    toastr.error( response.message );
                }
                else
                {   
        
                    toastr.success( response.message );
                    $scope.users=response.user_add;
                    $scope.members=response.members;
                    $scope.project.user_id = undefined;
                }

                $scope.formDisabled = false;
                
                });
                
            };

            this.reset_form = function(){
                $scope.contact = {
                    id:0,
                    names:'',
                    phone:'',
                    email:'',
                    default:0,
                };
            };

            this.remove = function( member ){
                var $parent = $scope;
                SweetAlert.swal({
                    title: "Are you sure you want to remove member?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    //cancelButtonText: 'Cancel',
                    confirmButtonColor: "#DD6B55",
                    //confirmButtonText: "Yes, Confirm remove!",
                    //closeOnConfirm: true
                }, function ( isConfirm ) {

                    if(isConfirm)
                    {   
                        //swal("Cita eliminada!", "La Cita fue eliminada.", "success");
                        $http.delete($APP.server('member/'+member.member_id+'/delete/')).success(function(response){
                                $scope.members.splice( $scope.members.indexOf(member),1);
                                $parent.users.push(member);
                                toastr.success( response.message );
                           
                         });
                        
                    }
                });
            };
        };

        /**
        |------------------------
        | tasks
        |------------------------
        **/
        var action_task = function(){
            
            var $self   = this;
            this.open_form = function(){
                var $parent = $scope;

                $uibModal.open({
                    templateUrl: $APP.view('project/views/modal-task'),
                    size: 'lg',
                    controller: function( $scope, $uibModalInstance, $http  ){
                        
                        $scope.list_members=$parent.members;
                        $scope.newTask = function()
                        {
                           $scope.formDisabled = true; 
                            $http.post( $APP.server('project/'+$parent.project.id+'/task/'), $scope.task ).success(function(response){
                                
                                if(!response.status )
                                {
                                    toastr.error( response.message );
                                }
                                else
                                {   
                                    toastr.success( response.message );
                                    $parent.tasks = response.tasks;
                                    $uibModalInstance.close();
                                }

                                $scope.formDisabled = false;
                                
                            });
                        }
                    }
                });
            };
            
            this.reset_form = function(){
                $scope.contact = {
                    id:0,
                    type:'1',
                    message:''
                };
            };
        };

        $scope.action_project_list = new action_project_list(); 
        $scope.action_task = new action_task();

    }
};

$APP.routes['project.tasks'] = {
    url: '/listTask',
    templateUrl: $APP.view('project/views/tasks'),
    controller: function( $scope, pagination, $http){

        $scope.filters = {
            project_name:'',
            nota:'',
            created_at:'',
            start_date:'',
            end_date:'',
            status:'',
            user_created:'',
            status:''      };

        $scope.pagination = new pagination('project/searchTask', $scope.filters );
        $scope.pagination.getData(1);

        var action_update_task = function(){
            
            var $self   = this;
            console.log($scope.pagination);
            this.update= function(task_id,value){
                datos={
                    task: task_id,
                    val: value
                }
                $http.post( $APP.server('project/updateTask/'), datos).success(function(response){        
                    if(!response.status )
                    {
                        toastr.error( response.message );
                    }
                    else
                    {   
                        toastr.success( response.message );
                        $scope.pagination.getData($scope.pagination.currentPage);
                    }
                    $scope.formDisabled = false;       
                });
            };
        };

        $scope.action_update_task = new action_update_task();
    }
};