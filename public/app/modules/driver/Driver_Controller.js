$APP.routes['driver'] = {
    url:'/driver',
    template: '<div ui-view ></div>'
}

$APP.routes['driver.list'] = {
    url: '/list',
    templateUrl: $APP.view('driver/views/search'),
    controller: function( $scope, pagination ){
        
        $scope.filters = {
            name:'',
            model:''     };

        $scope.pagination = new pagination('driver/search', $scope.filters );
        $scope.pagination.getData(1);
    }
};

$APP.routes['driver.new_driver'] = {
    url: '/create',
    templateUrl: $APP.view('driver/views/create'),
    controller: function($scope, $http , $state ) {
        
        $scope.driver = {
            name:'',
            cargo:'',     };
        
        $scope.formDisabled = false;

        $scope.form_submit_create_driver = function() {
            $scope.formDisabled = true;

            $http.post( $APP.server('driver/create/'), $scope.driver ).success(function( response ){

                if(!response.status )
                {   
                    toastr.error( response.message );
                    $scope.formDisabled = false;

                }
                else
                {
                    toastr.success( response.message );
                    $state.go('driver.edit', { 'id' : response.driver.id } );
                }

               
            })

        }
    }
};

$APP.routes['driver.edit'] = { 
    url: '/:id/edit',
    templateUrl: $APP.view('driver/views/edit'),
    controller: function( $scope , $http, $stateParams , $uibModal, SweetAlert, $state  ) {

        $state.driver_name = '';

        $http.get($APP.server('driver/'+$stateParams.id+'/info/')).success( function( response ){
            $scope.driver = response.driver;
            $state.driver_name = response.driver.names;
        });
        
        $scope.formDisabled = false;
        
        $scope.form_submit_update_driver = function() {
            
            $scope.formDisabled = true;
            
            $http.post( $APP.server('driver/'+$scope.driver.id+'/update/'), $scope.driver).success(function(response){
                
                if(!response.status )
                {
                    toastr.error( response.message );
                }
                else
                {
                    toastr.success( response.message );
                }

                $scope.formDisabled = false;
                
            });
        }

    }
};