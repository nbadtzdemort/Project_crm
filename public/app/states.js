


angular.module($APP.name)
    .config(function( $stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $httpProvider) {
        
        /**
        ** send headers post always utf 8 encoded
        **/
        $httpProvider.defaults.headers.post = {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        };
        $httpProvider.defaults.transformRequest = function(data){
            if (data === undefined) {
                return data;
            }
            return $.param(data);
        };
        
        /**
        ** Url Default
        **/
        $urlRouterProvider.otherwise("/");
        
        $ocLazyLoadProvider.config({
            debug: false
        });
        
        for(var route in $APP.routes){
        	
        	prop = $APP.routes[route];

        	$stateProvider.state(route, prop);;
            
        }
        
    }).run(function($rootScope, $state ) {
        
        $state.elapsed_time = function(d)
        {
            return moment.utc(d, "YYYY-MM-DD hh:mm:ss").fromNow()
        }

        $state.human_time = function(d)
        { 
            return moment.utc(d, "YYYY-MM-DD hh:mm:ss").format('lll');
        }

        $state.mask_phone = function( number )
        {
            
            if(typeof number == "object")
            {
                return '';
            }
            else if(number.toString().length!= 10 )
            {
                return '';
            }
            
            var lada_number = number.match(/(\d{3})(\d{3})(\d{4})/);
            return "(" + lada_number[1] + ") " + lada_number[2] + "-" + lada_number[3];
        }
        
        $rootScope.$state = $state;
        
    });

