/**
|------------------------------
|	App Global Settings
|------------------------------
|
**/
var $APP = {
	
	name: 'myApp',
	
	version: Math.random().toString(36).substr(4, 20),

	domain: document.head.querySelector('[name="domain"]').content,

	view: function(dir)
	{	
		return this.domain + 'app/modules/' + dir + '.html?v=1'
	},

	server: function(url)
	{
		return this.domain + url;
	},

	routes:{}
}

/**
|------------------------------
|	Angular init
|------------------------------
|
**/
angular
	.module("myApp", [
		'ui.router',
		'oc.lazyLoad',
		'ngSanitize',
		'angularUtils.directives.dirPagination',
		'ui.select',
		'ui.bootstrap',
		'oitozero.ngSweetAlert',
		'ui.mask'
	])
	.controller('MainCtrl',function( $scope ){
		$scope.$APP = $APP;
	});

/**
|------------------------------
|	Save toastr options
|------------------------------
|
**/
toastr.options = {
    "closeButton": true,
    "debug": false,
    "progressBar": true,
    "preventDuplicates": false,
    "positionClass": "toast-top-right",
    "onclick": null,
    "showDuration": "400",
    "hideDuration": "1000",
    "timeOut": "7000",
    "extendedTimeOut": "3000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}
