
angular
    .module($APP.name)
    .directive('pageTitle', function pageTitle($rootScope, $timeout) {
	    return {
	        link: function(scope, element) {
	            var listener = function(event, toState, toParams, fromState, fromParams) {
	                // Default title - load on Dashboard 1
	                var title = 'GST | Companies Relation Managment';
	                // Create your own title pattern
	                if (toState.data && toState.data.pageTitle) title = 'CRM | ' + toState.data.pageTitle;
	                $timeout(function() {
	                    element.text(title);
	                });
	            };
	            $rootScope.$on('$stateChangeStart', listener);
	        }
	    };
	})
	.directive('icheck',function($timeout) {
	    return {
	        restrict: 'A',
	        require: 'ngModel',
	        link: function($scope, element, $attrs, ngModel) {
	            return $timeout(function() {
	                var value;
	                value = $attrs['value'];

	                $scope.$watch($attrs['ngModel'], function(newValue){
	                    $(element).iCheck('update');
	                })

	                return $(element).iCheck({
	                    checkboxClass: 'icheckbox_square-green',
	                    radioClass: 'iradio_square-green'

	                }).on('ifChanged', function(event) {
	                        if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
	                            $scope.$apply(function() {
	                                return ngModel.$setViewValue(event.target.checked);
	                            });
	                        }
	                        if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
	                            return $scope.$apply(function() {
	                                return ngModel.$setViewValue(value);
	                            });
	                        }
	                    });
	            });
	        }
	    };
	})
	.directive('companyName', function companyName($rootScope, $timeout) {
	    return {
	        link: function(scope, element) {
	            var listener = function(event, toState, toParams, fromState, fromParams) {
	                // Default title - load on Dashboard 1
	                var title = 'Details';
	                // Create your own title pattern
	                if (toState.data && toState.data.companyName) title =  toState.data.companyName;
	                $timeout(function() {
	                    element.text(title);
	                });
	            };
	            $rootScope.$on('$stateChangeStart', listener);
	        }
	    };
	})
	.directive('footable', function() {
            var events = {
                beforeFiltering: 'footable_filtering'
            };
            var extractSpecOpts = function(opts, attrs) {
                var extracted = {},
                    k;
                for (k in opts) {
                    if (k !== 'filter' && (!angular.isUndefined(events[k]))) {
                        if(!angular.isFunction(scope.$eval(attrs[k]))) {
                            extracted[k] = attrs[k];
                        }
                    }
                }
                return extracted;
            };

            var bindEventHandler = function(tableObj, scope, attrs) {
                var k;
                for (k in attrs) {
                    if (k !== 'filter' && (!angular.isUndefined(events[k]))) {
                        var targetEventName = events[k];
                        if(angular.isFunction(scope.$eval(attrs[k]))) {
                            tableObj.bind(targetEventName, scope.$eval(attrs[k]));
                        }
                    }
                }
            };

            return {
                restrict: 'C',
                link: function(scope, element, attrs) {
                    var tableOpts = {
                        'event-filtering': null
                    };

                    angular.extend(
                        tableOpts,
                        footable.options
                    );

                    angular.extend(
                        tableOpts,
                        extractSpecOpts(tableOpts, attrs)
                    );

                    var tableObj = element.footable(tableOpts);

                    bindEventHandler(tableObj, scope, attrs);

                }
            };
        })

	.directive("datepicker", function(){
		return {
		    require: "?ngModel",
		    scope: true,
		    //template: "<input ng-model='value' class='form-control' ng-change='onChange()'>",
	    	link: function(scope, element, attrs, ngModel){
	      		
	      		if (!ngModel) return;
	      		
	      		$( element ).bind('keydown', function (event) {
		            if (event.which == 13) {
		                var e = jQuery.Event("keydown");
						e.which   = 9;//tab 
						e.keyCode = 9;
		                $(this).trigger(e);
		                return false;
		            }
				}).datepicker({
			        format: 'mm/dd/yyyy',
			        language: 'es',
			        autoclose: true,
			        toggleActive: true,
			        todayHighlight: true,
			        todayBtn: false,
			        zIndexOffset: 1040
			    });
				
	      		scope.onChange = function(){
	        		ngModel.$setViewValue(scope.value);
	      		};

	      		ngModel.$render = function(){
	      			$(element).val(ngModel.$modelValue);
	      		};
	    	}
	  	};
	});