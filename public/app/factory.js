angular.module($APP.name)
    .factory('pagination', function( $http ) {
    	
    	var pagination = function( url , filters  ) { 
            
            var $self = this;
            
            this.url            = url;
            this.filters        = filters;
            
            this.currentPage    = 1;
            this.total_count    = 0;
            this.itemsPerPage   = 15;
            this.numQuery       = 0;
            this.lastQuery      = false;
            this.loadingQuery   = false;
            this.result_data    = [];
            this.preventLoading = false;
            
            this.sort = {
                name:'id',
                type:'desc',
            }

            /*
            |-------------------------------------------------
            |   Include 3 avalaible functions
            |-------------------------------------------------
            |
            |   $self.preQuery = function(){} #returns boolean
            |   $self.postQuery = function( response  ){}	
            |   $self.finally = function(){}
            |
            */

            $self.getData = function(pageno)
            {
                
                var numQuery = ++$self.numQuery;

                $self.lastQuery   = true;
                $self.currentPage = pageno;

                var filters = angular.copy($self.filters);

                var sort = {
                    name: $self.sort.name,
                    type: $self.sort.type
                }

                var $data = {
                    filters: filters,
                    sort: sort
                }

                var full_url = $APP.domain + "" + $self.url + "/" + $self.itemsPerPage+"/"+pageno+"?"+ $.param($data);
                
                if(typeof $self.preQuery === 'function')
                {
                    if( !$self.preQuery() )
                    {
                    	return false;
                    }
                }

                $http.get( full_url  ).success(function(response){ 
                    
                    if(numQuery === $self.numQuery && $self.lastQuery)
                    {   
                        
                        $self.result_data  = response.result_data;
                        $self.total_count  = response.total_count;
                        $self.lastQuery    = false;
                        $self.loadingQuery = false;

                    }
                    else if($self.lastQuery && $self.preventLoading )
                    {   
                        $self.loadingQuery = true;
                        $self.result_data  = [];
                    }

                    if(typeof $self.postQuery === 'function')
                    {
                        $self.postQuery( response );
                    }
                }).finally(function(){
                    
                    if(typeof $self.finally === 'function')
                    {
                        $self.finally();
                    }

                    setTimeout(function(){
                        $('[data-toggle="tooltip"]').tooltip();
                    },1);
                });
            };
            
            $self.sortData = function( name )
            {
                $self.sort.name = name;
                $self.sort.type = ($self.sort.type=='asc') ? 'desc' : 'asc';
                $self.getData(1);
            };

            $self.sortClass = function(keyName)
            {
                if(keyName != $self.sort.name)
                {
                    return 'fa-sort';
                }
                else if($self.sort.type=== 'asc')
                {
                    return 'fa-sort-asc'
                }
                else
                {
                    return 'fa-sort-desc'
                }
            }
        }
        
       	return pagination;

    });
        