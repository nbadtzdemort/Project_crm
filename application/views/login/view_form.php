<div style='border-bottom:15px solid #1ab394; border-radius:15px;'>
	<div style='width: 100%;'><h1 class="logo-name" style='font-size: 140px;'>CRM</h1></div>
	<h3 style='border:transparent 20px solid;border-top:8px solid #1ab394; border-radius:0px;color:#ccc;'><br>Welcome to CRM</h3>
	<p style='color:white'>Web App perfectly designed to manage your projects and work teams, taking control of all the tasks of your company.</p>
	<p style='color:white'>Login in or register to begin.</p>
	<form method="post" class="m-t" role="form" action="<?=base_url("/login/intent")?>">
		<div class="form-group">
		    <input type="email" class="form-control" placeholder="Username" required="" name="email">
		</div>
		<div class="form-group">
		    <input type="password" class="form-control" placeholder="Password" required="" name="password">
		</div>
		<button type="submit" class="btn btn-primary block full-width m-b">Login</button>
	</form><br>
	<p style='color:white' class="m-t">Created By | <a href="http://greenshieldtech.com" style='color:#1ab394'>Green Shield Technology &copy; 2017 </a> </p>
</div>