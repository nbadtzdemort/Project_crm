<?php

class APP_Controller extends CI_Controller
{	
	public $current_user;
	function __construct()
	{
		
		parent::__construct();

		$this->template->layout('admin');

		/**
		 * Remember remove this secition in production
		 */
		if( ! ($this->current_user = @$this->session->userdata['User_DB'] ) )
		{	
			$this->template->setNotify('error', 'Please try login again');
			redirect('/');
		}
		
	}
}