<?php
/**
* 
*/
class Install
{

	private  $config;

	function __construct()
	{
		
		$this->CI =& get_instance();
			
		$config = require 'Config.php';
		
		$this->config = $config;
	}

	public function check()
	{
		
		$this->checkModules();

		$this->checkTables();

		$this->checkUser();
	}

	private function checkModules()
	{
		$moduleErrors = [];

		foreach ($this->config['modules'] as $module ) {
			if(!extension_loaded($module))
			{
				$moduleErrors[] = "Module [mysqlnd] is required php7.0-".$module;
			}
		}

		if(count($moduleErrors) > 0 )
		{	
			show_error(implode("<br>",$moduleErrors) , 500 , "Modules not found");
		}
	}
	
	private function checkTables()
	{
		if(!isset($this->CI->db))
		{
			show_error("Config/autoload.php enable database library",505);
		}
		if(!function_exists('redirect'))
		{
			show_error("Config/autoload.php enable helper url",505);
		}

		$tables = $this->CI->db->list_tables();
		if(!in_array("user",$tables))
		{
			$this->addTableUser();
		}
	}
	
	private function addTableUser()
	{

		$userFields = require 'table.user.php';

		$this->CI->load->dbforge();	
		
		$this->CI->dbforge->add_key('id', true);

		$this->CI->dbforge->add_field($userFields );

		$this->CI->dbforge->create_table( 'user', true );
	}

	private function checkUser()
	{
		$defaultUser = $this->CI->db->get_where('user', [
			'email' => $this->config['user_default']['email']
		]);

		if(!$defaultUser->row())
		{	
			$params = $this->config['user_default'];


			$params['password'] = password_hash(  $params['password'] , PASSWORD_BCRYPT );

			$this->CI->db->insert('user' , $params );
		}
	}

}