<?php
return [
        'id' => array(
                'type' => 'BIGINT(20)',
                'unsigned' => true,
                'auto_increment' => true,
        ),
        'email' => array(
                'type' => 'VARCHAR(250)',
                'unique' => true,
        ),
        'password' => array(
                'type' => 'VARCHAR(500)',
                'default' => ''
        ),
        'names' => array(
                'type' => 'VARCHAR(250)',
                'default' => ''
        ),
        'role' => array(
                'type' => 'VARCHAR(75)',
                'default' => 'collaborator'
        ),
        'status' => array(
                'type' => 'SMALLINT(1)',
                'default' => 0
        ),
];

