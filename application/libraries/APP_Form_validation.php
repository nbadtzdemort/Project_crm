<?php 
class APP_Form_validation extends CI_Form_validation{
    
    function __construct( $rules = array() )
	{
	    parent::__construct( $rules );

        $this->set_message('is_unique_multiple','The field {field} entered already exists');
	}
	
	public function is_unique_multiple($str, $serializeWhere)
	{
		$field = unserialize($serializeWhere);
		
		if(isset($field['table']) || $field['table']==='')
		{
			$this->set_message('is_unique_multiple','is_unique_multiple in field {field} required serialize value [table]');
			return  FALSE;
		}
		else if(isset($field['where']) || !count($field['where']))
		{
			$this->set_message('is_unique_multiple','is_unique_multiple in field {field} required serialize values [where]');
			return  FALSE;
		}

		return isset($this->CI->db)
			? ($this->CI->db->limit(1)->get_where($field['table'], $field['where'] )->num_rows()  === 0)
			: FALSE;
	}
	
	public function valid_date( $str ) 
    {
    	$date = @date_parse( $str ); 
        
        if ($date["error_count"] == 0 && checkdate($date["month"], $date["day"], $date["year"]))
            return TRUE;
        else
        {   
            $this->set_message('valid_date', 'Field {field} is not valid date');
            return FALSE;
        }
    }

    public function date_max_today( $str)
    {
        $date = @date_parse( $str ); 
        
        if ($date["error_count"] == 0 && checkdate($date["month"], $date["day"], $date["year"]))
        {
            $today         = (int)date('Ymd');
            $date_selected = (int)date('Ymd',strtotime($str));
            
            if($date_selected > $today )
            {   
                $this->set_message('date_max_today', 'The {field} field can not be greater than the current date');
                return FALSE;
            }
        }
        
        return TRUE;
    }

    public function date_min_today( $str)
    {
        $date = @date_parse( $str ); 
        
        if ($date["error_count"] == 0 && checkdate($date["month"], $date["day"], $date["year"]))
        {
            $today         = (int)date('Ymd');
            $date_selected = (int)date('Ymd',strtotime($str));
            
            if($date_selected < $today )
            {   
                $this->set_message('date_min_today', 'The {field} field can not be less than the current date');
                return FALSE;
            }
        }
        
        return TRUE;
    }
    
    public function exist_data( $str, $field )
    {
        sscanf($field, '%[^.].%[^.]', $table, $field);
            
        if( ($this->CI->db->limit(1)->get_where($table, array($field => $str))->num_rows() === 0) )
        {   
            $this->set_message('exist_data',  "The field {field} not exist");
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    
}
