<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
		
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title page-title></title>

        <link rel="shortcut icon" type="image/png" href="<?= base_url('favicon.png')?>"/>

        <meta charset="utf-8" content="<?php echo site_url();?>" name="domain">

        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/bootstrap.min.css'); ?>" >
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css'); ?>" >

        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/plugins/fullcalendar/fullcalendar.css'); ?>" >
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/plugins/datapicker/datepicker3.css'); ?>" >

        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/plugins/toastr/toastr.min.css'); ?>" >
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/js/plugins/gritter/jquery.gritter.css'); ?>" >

        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/plugins/ui-select/ui-select.min.css'); ?>" >

        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/plugins/iCheck/custom.css'); ?>" >

        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/animate.css'); ?>" >
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/plugins/sweetalert/sweetalert.css'); ?>" >
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/plugins/ladda/ladda-themeless.min.css'); ?>" >
        
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/fonts/fonts-google/google.opensans.css'); ?>" >

        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/style.css'); ?>" >
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>

        <?php echo $config['css']; ?>
        <meta name="robot" content="noindex, nofollow" />

        <title><?php echo $config['title'] ?></title>
        
        <?php echo $config['css']; ?>
       	
    </head> 
    <body class="" cz-shortcut-listen="true" style='background:url("<?=base_url()."/assets/img/admin.jpg";?>"); background-size: cover'>
       <div style='background:rgba(47, 64, 80,0.8); height: 100%;'>
        <?php
            echo($config['modals']);
        ?>
        
        <div class="middle-box text-center loginscreen animated fadeInDown" >
            <?php  $this->load->view( $config['view'] , ['_' => $_ ]  );  ?>
        </div> 
		<?php echo $config['js']; ?>
    </div>
</body>
</html>

