<!DOCTYPE html>
<html ng-app="myApp">
	<head>

	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">

	    <!-- Page title set in pageTitle directive -->
	    <title page-title></title>

	    <link rel="shortcut icon" type="image/png" href="<?= base_url('favicon.png')?>"/>

	    <meta charset="utf-8" content="<?php echo site_url();?>" name="domain">

	    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/bootstrap.min.css'); ?>" >
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css'); ?>" >

		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/plugins/fullcalendar/fullcalendar.css'); ?>" >
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/plugins/datapicker/datepicker3.css'); ?>" >

		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/plugins/toastr/toastr.min.css'); ?>" >
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/js/plugins/gritter/jquery.gritter.css'); ?>" >

		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/plugins/ui-select/ui-select.min.css'); ?>" >

		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/plugins/iCheck/custom.css'); ?>" >

		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/animate.css'); ?>" >
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/plugins/footable/footable.core.css'); ?>" >
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/plugins/sweetalert/sweetalert.css'); ?>" >
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/plugins/ladda/ladda-themeless.min.css'); ?>" >
		
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/fonts/fonts-google/google.opensans.css'); ?>" >

		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/style.css'); ?>" >
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets\css\plugins\jasny\jasny-bootstrap.min.css'); ?>" >
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>

	    <?php echo $config['css']; ?>

	</head>
	
	<!-- ControllerAs syntax -->
	<body  <?= $config['body'] ?> >
	    <div id="wrapper" >
	        <nav class="navbar-default navbar-static-side" role="navigation">
	            <div class="sidebar-collapse">
	                <ul class="nav metismenu" id="side-menu">
	                    <li class="nav-header">
	                        <div class="dropdown profile-element"> 
	                        	<span><img alt="image" class="img-rounded" src="<?= base_url('logo.png'); ?>" /></span>
	                            <a data-toggle="dropdown" class="dropdown-toggle" href="">
		                            <span class="clear"> 
		                            	<span class="block m-t-xs"> 
		                            		<strong class="font-bold"><?= $this->current_user->names; ?></strong>
		                             	</span> 
		                             	<span class="text-muted text-xs block"><?= $this->current_user->role ?> <b class="caret"></b></span> 
		                             </span> 
	                            </a>
	                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
	                                <li><a ui-sref="profile">Profile</a></li>
	                                <li class="divider"></li>
	                                <li><a href="<?php echo site_url('/login/close')?>">Log out</a></li>
	                            </ul>
	                        </div>
	                        <div class="logo-element">CRM</div>
	                    </li>

	                   	<li  ui-sref-active="active">
	                        <a ui-sref="inicio">
	                            <i class="fa fa-th-large"></i> 
	                            <span class="nav-label">Dashboard</span>
	                        </a>
	                    </li>
	                    <li ng-class="{active: $state.includes('company')}" >
	                        <a href="">
	                            <i class="fa fa-briefcase"></i> 
	                            <span class="nav-label">Companies</span>
	                            <span class="fa arrow"></span>
	                        </a>
	                        <ul class="nav nav-second-level collapse" ng-class="{in: $state.includes('company')}">
	                            <li ng-show="$state.includes('company.edit')"  ui-sref-active="active" >
	                            	<a  ui-sref="company.edit" class="disabled"> {{ $state.company_name }} <span class="pull-right label label-default"> DETAIL </span></a></li>
	                            <li ui-sref-active="active" ><a ui-sref="company.search" > Search  </a></li>
	                            <li ui-sref-active="active" ><a ui-sref="company.create" > New </a></li>
	                            <li ui-sref-active="active" ><a ui-sref="company.settings" > Settings </a></li>
	                        </ul>
	                    </li>
	                    <li ng-class="{active: $state.includes('project')}" >
	                        <a href="">
	                            <i class="fa fa-indent"></i> 
	                            <span class="nav-label">Projects</span>
	                            <span class="fa arrow"></span>
	                        </a>
	                        <ul class="nav nav-second-level collapse" ng-class="{in: $state.includes('project')}">
	                        	<li ng-show="$state.includes('project.edit')"  ui-sref-active="active" >
	                            	<a  ui-sref="project.edit" class="disabled"> {{ $state.project_name }} <span class="pull-right label label-default"> DETAIL </span></a>
	                           	</li>
	                            <li ui-sref-active="active" ><a ui-sref="project.list" > Search  </a></li>
	                            <li ui-sref-active="active" ><a ui-sref="project.new" > New </a></li>
	                            <li ui-sref-active="active" ><a ui-sref="project.tasks" > Tasks  </a></li>
	                            <li ui-sref-active="active" ><a ui-sref="project.settings" > Settings </a></li>
	                        </ul>
	                    </li>
	                    <li  ui-sref-active="active">
	                        <a ui-sref="users">
	                            <i class="fa fa-users"></i> 
	                            <span class="nav-label">Users</span>
	                            <span class="label label-primary pull-right" id="total_users"><?= $_['total_users']?></span>
	                        </a>
	                    </li>
	                    <li ng-class="{active: $state.includes('car')}" >
	                        <a href="">
	                            <i class="fa fa-car"></i> 
	                            <span class="nav-label">Car</span>
	                            <span class="fa arrow"></span>
	                        </a>
	                        <ul class="nav nav-second-level collapse" ng-class="{in: $state.includes('car')}">
	                        	<li ng-show="$state.includes('car.edit')"  ui-sref-active="active" >
	                            	<a  ui-sref="project.edit" class="disabled"> {{ $state.car_name }} <span class="pull-right label label-default"> DETAIL </span></a>
	                           	</li>
	                            <li ui-sref-active="active" ><a ui-sref="car.list" > Cars  </a></li>
	                            <li ui-sref-active="active" ><a ui-sref="car.new_car" > New </a></li>
	                            <li ui-sref-active="active" ><a ui-sref="car.control" > Control </a></li>
	                            <li ui-sref-active="active" ><a ui-sref="car.settings" > Settings </a></li>
	                        </ul>
	                    </li>
	                    <li ng-class="{active: $state.includes('driver')}" >
	                        <a href="">
	                            <i class="fa fa-id-card"></i> 
	                            <span class="nav-label">driver</span>
	                            <span class="fa arrow"></span>
	                        </a>
	                        <ul class="nav nav-second-level collapse" ng-class="{in: $state.includes('driver')}">
	                        	<li ng-show="$state.includes('driver.edit')"  ui-sref-active="active" >
	                            	<a  ui-sref="project.edit" class="disabled"> {{ $state.driver_name }} <span class="pull-right label label-default"> DETAIL </span></a>
	                           	</li>
	                            <li ui-sref-active="active" ><a ui-sref="driver.list" > drivers  </a></li>
	                            <li ui-sref-active="active" ><a ui-sref="driver.new_driver" > New </a></li>
	                            <li ui-sref-active="active" ><a ui-sref="driver.settings" > Settings </a></li>
	                        </ul>
	                    </li>
	                    <li ng-class="{active: $state.includes('Log')}" >
	                        <a href="">
	                            <i class="fa fa-th-list"></i> 
	                            <span class="nav-label">Logs</span>
	                            <span class="fa arrow"></span>
	                        </a>
	                        <ul class="nav nav-second-level collapse" ng-class="{in: $state.includes('log')}">
	                            <li ui-sref-active="active" ><a ui-sref="log.myLog" > My Logs </a></li>
	                            <li ui-sref-active="active" ><a ui-sref="log.allLog" > All Logs </a></li>
	                        </ul>
	                    </li>
	                </ul>

	            </div>
	        </nav>

	        <!-- 
	        	.gray-bg
	        	.white-bg
	        	.navy-bg
	        	.blue-bg
	        	.lazur-bg
	        	.yellow-bg
	        	.red-bg
	        	.black-bg
	        -->
	        
	        <div id="page-wrapper" class="gray-bg dashbard-1">
	            <div class="row border-bottom">
	                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
	                    <div class="navbar-header">
	                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href=""><i class="fa fa-bars"></i> </a>
	                    </div>
	                    <ul class="nav navbar-top-links navbar-right ">
	                        <li ><a href="" ng-click="main.modal_cliente_nuevo()" > ... </a></li>
	                    </ul>
	                </nav>
	            </div>
	            <?php $this->load->view($config['view']); ?>
	        </div>
	    </div>
	    
		<!-- Wrapper-->
		
		<!-- End wrapper-->

		<script src="<?= base_url('assets/js/jquery-2.1.1.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/jquery-ui-1.10.4.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/jquery-ui.custom.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/plugins/jquery-ui/jquery-ui.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/plugins/metisMenu/jquery.metisMenu.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/plugins/slimscroll/jquery.slimscroll.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/bootstrap.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/plugins/datapicker/bootstrap-datepicker.js')?>"   ></script>
	    <script src="<?= base_url('\assets\js\plugins\jasny\jasny-bootstrap.min.js')?>"   ></script>

	    <script src="<?= base_url('assets/js/plugins/fullcalendar/moment.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/inspinia.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/plugins/pace/pace.min.js')?>"   ></script>
	  

	    <script src="<?= base_url('assets/js/plugins/gritter/jquery.gritter.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/plugins/toastr/toastr.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/plugins/ladda/spin.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/plugins/ladda/ladda.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/plugins/ladda/ladda.jquery.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/plugins/sweetalert/sweetalert.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/plugins/iCheck/icheck.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/plugins/footable/footable.all.min.js')?>"   ></script>
	    
	    <!-- AngularJS -->
	    <script src="<?= base_url('assets/js/angular/angular.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/angular/angular-ui-router.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/angular/angular-ocLazyLoad.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/angular/angular-ngSanitize.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/angular/angular-dirPagination.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/angular/angular-ui-select.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/angular/angular-ui-bootstrap.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/angular/angular-ui-bootstrap-tpls.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/angular/angular-sweetalert.min.js')?>"   ></script>
	    <script src="<?= base_url('assets/js/angular/angular-mask.js')?>"   ></script>

	    <?php echo $config['js']; ?>
	</body>
</html>