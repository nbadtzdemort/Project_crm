<?php
class Contact_List_Model extends APP_Model
{
    function changeDefault( $company_id , $contact_id )
    {
    	//#disable current default
    	$this->db->where( ['company_id' => $company_id , 'default' => 1 ] )
        ->update($this->table, [
        	'default' => 0
        ]);
        
        //#enabled new default
        $this->db->where( ['id' => $contact_id ] )
        ->update($this->table, [
        	'default' => 1
        ]);
        
        return true;
    }
}
