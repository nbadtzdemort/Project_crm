<?php

class Driver_Model extends APP_Model
{
	public function getSearch( $limit,  $start, $data = null ) 
    {
    	$fieldsSort = [
    		'id' => 'driver.id',
            'name' => 'driver.names',
            'cargo' => 'driver.cargo'
    	];

    	$basicFields = [
			'driver.id',
            'driver.names',
            'driver.cargo'
		];

		$where = isset( $data['filters'] ) ?  $data['filters'] : null;
 
		$this->_filter( $where ); 

        $this->db->select( $basicFields )
        	->from('driver');

    	
       	$this->db->limit(abs($limit),abs($start) );

        $sortName = isset( $data['sort']['name']) ? $data['sort']['name'] : '';
        $sortType = isset( $data['sort']['type']) ? $data['sort']['type'] : '';

        
        if( isset($fieldsSort[$sortName]) && in_array($sortType, ['asc','desc']) )
        {	
             
			$this->db->order_by($fieldsSort[$sortName].' '.$data['sort']['type']);
        }
        else
        {
        	$this->db->order_by('driver.id');
        }
       	
        $result_data = $this->db->get()->result();    	
    	
    	$this->_filter( $where );

        $this->db->from('driver');

        $total_count = $this->db->count_all_results();	
    	
    	return [
    		'total_count' => $total_count,
    		'result_data' => $result_data
    	];
    }

    private function _filter( $where = null ) 
    {
        if($name = $this->filterValue($where,'name'))
        {
            
            $this->db->like( [ 'name' => $name ]  );
        }
        if($model = $this->filterValue($where,'model'))
        {
            $this->db->like( [ 'model' => $model ] );
        }
        
    }

}
