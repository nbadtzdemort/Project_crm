<?php

class Project_Model extends APP_Model
{
    public function getSearch( $limit,  $start, $data = null ) 
    {
        $fieldsSort = [
            'id' => 'project.id',
            'name' => 'project.name',
            'create_date' => 'project.create_date',
            'end_date' => 'project.end_date',
            'descrition' => 'project.description',
        ];

        $basicFields = [
            'project.id',
            'project.name',
            'project.create_date',
            'project.end_date',
            'project.description',
        ];

        $where = isset( $data['filters'] ) ?  $data['filters'] : null;
 
        $this->_filter( $where ); 

        $this->db->select( $basicFields )
            ->from('project');

        
        $this->db->limit(abs($limit),abs($start) );

        $sortName = isset( $data['sort']['name']) ? $data['sort']['name'] : '';
        $sortType = isset( $data['sort']['type']) ? $data['sort']['type'] : '';

        
        if( isset($fieldsSort[$sortName]) && in_array($sortType, ['asc','desc']) )
        {   
             
            $this->db->order_by($fieldsSort[$sortName].' '.$data['sort']['type']);
        }
        else
        {
            $this->db->order_by('project.id');
        }
        
        $result_data = $this->db->get()->result();      
        
        $this->_filter( $where );

        $this->db->from('project');

        $total_count = $this->db->count_all_results();  
        
        return [
            'total_count' => $total_count,
            'result_data' => $result_data
        ];
    }

    private function _filter( $where = null ) 
    {
        if($name = $this->filterValue($where,'name'))
        {
            
            $this->db->like( [ 'name' => $name ]  );
        }
        if($create_date = $this->filterValue($where,'create_date'))
        {
            $this->db->like( [ 'create_date' => $create_date ] );
        }
        if($end_date = $this->filterValue($where,'end_date'))
        {
            $this->db->like( [ 'end_date' => $end_date ] );
        }
    }

}
