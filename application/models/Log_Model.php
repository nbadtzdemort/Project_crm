<?php

class Log_Model extends APP_Model
{
    private $basicFields = [
            'log.date',
            'log.message',
            'log.action',
            'log.type',
            'user.names',
        ];

   public function log($action,$message, $user, $type){
        $data = array(
        'user_id' => $user,
        'date'    => date("Y-m-d H:i:s"),
        'message' => $message,
        'action'  => $action,
        'type'    => $type
        );
        $this->db->insert('log', $data);
   }

   public function log_list($field,$value,$id){
            $this->db->select($this->basicFields)
                     ->from("log")
                     ->join("user","log.user_id=user.id","inner");
                        if($id>0)
                        {
                        $this->db->where('user_id',$id);
                        }
                        if($field!='')
                        {
                            if($field=='user_id')
                            {
                                $this->db->like("user.names",$value);
                            }
                            else
                            {
                                $this->db->like($field,$value);
                            }
                        }
                     $this->db->order_by('date', 'DESC');
            return $this->db->get()->result();
   }

}

 