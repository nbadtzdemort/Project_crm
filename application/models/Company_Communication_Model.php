<?php
/**
* 
*/
class Company_Communication_Model extends APP_model
{
	private $available_status = [
		1 => 'Call',
		2 => 'Regular',
		3 => 'Notes'
	];

	private $_fields = [
		'company_communication.id',
		'company_communication.created_at',
		'company_communication.type',
		'company_communication.message',
		'company_communication.company_id',
		'company_communication.user_id',
		'user.names as user'
	];

	public function getAvailableStatus()
	{
		return $this->available_status;
	}

	public function getDetail( $id )
	{
		$this->db->select($this->_fields)->from("company_communication")
			->join('user', 'user.id=company_communication.user_id','inner')
			->where(["company_communication.id" => $id ])
		;
		
		return $this->db->get()->row();
	}

	public function getAllByCompany( $company_id )
	{
		$this->db->select($this->_fields)->from("company_communication")
			->join('user', 'user.id=company_communication.user_id','inner')
			->where(["company_communication.company_id" => $company_id ])	
			->order_by("company_communication.created_at", 'DESC')
		;
		
		return $this->db->get()->result_array();
	}

	public function getLast( $number = 5 )
	{
		$fields = array_merge($this->_fields, [
			'company_communication.company_id',
			'company.name as company_name'
		]);
		
		$this->db->select($fields)->from("company_communication")
			->join('user', 'user.id=company_communication.user_id','inner')
			->join('company', 'company.id=company_communication.company_id','inner')
			->order_by("company_communication.created_at", 'DESC')
		;

		$this->db->limit( $number );

		return $this->db->get()->result_array();
	}
}