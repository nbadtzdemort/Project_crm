<?php

class Company_Model extends APP_Model
{

    private $available_status = [
        1 => 'Lead',
        2 => 'Customer',
        3 => 'Discard'
    ];

	public function getSearch( $limit,  $start, $data = null ) 
    {
    	$fieldsSort = [
            'id'      => 'company.id',
            'name'    => 'company.name',
            'address' => 'company.address',
            'contact' => 'contact_list.names',
            'owner'   => 'user.names',
            'status'  => 'company.status',
    	];

    	$basicFields = [
			'company.id',
            'company.name',
            'company.address',
            'company.status',
            'contact_list.names as contact_name',
            'contact_list.phone as contact_phone',
            'contact_list.email as contact_email',
            'user.names as owner',
		];
        
		$where = isset( $data['filters'] ) ?  $data['filters'] : null;
    

        //########## QUERY RESULTS #############//
        $this->db->select( $basicFields )
        	->from('company')
            ->join('user','user.id=company.user_owner','inner')
            ->join('contact_list','contact_list.company_id=company.id and contact_list.default=1','left');
        $this->_filter( $where ); 
    	
       	$this->db->limit(abs($limit),abs($start) );

        $sortName = isset( $data['sort']['name']) ? $data['sort']['name'] : '';
        $sortType = isset( $data['sort']['type']) ? $data['sort']['type'] : '';

        
        if( isset($fieldsSort[$sortName]) && in_array($sortType, ['asc','desc']) )
        {	
             
			$this->db->order_by($fieldsSort[$sortName].' '.$data['sort']['type']);
        }
        else
        {
        	$this->db->order_by('company.id');
        }
       	
        $result_data = $this->db->get()->result();    	
    	
        //########## QUERY COUNT #############// 
        $this->db->from('company')
            ->join('user','user.id=company.user_owner','inner')
            ->join('contact_list','contact_list.company_id=company.id and contact_list.default=1','left');
        $this->_filter( $where );
        
        $total_count = $this->db->count_all_results();	
    	
    	return [
    		'total_count' => $total_count,
    		'result_data' => $result_data
    	];
    }
    
    private function _filter( $where = null ) 
    {
        if($name = $this->filterValue($where,'name'))
        {
            $this->db->like( [ 'company.name' => $name ]  );
        }
        if($address = $this->filterValue($where,'address'))
        {
            $this->db->like( [ 'company.address' => $address ] );
        }

        if($contact = $this->filterValue($where,'contact'))
        {
            $this->db->like( [ 'contact_list.names' => $contact ] );
        }

        if($owner = $this->filterValue($where,'owner'))
        {
            $this->db->like( [ 'user.names' => $owner ] );
        }

        $status = $this->filterValue($where,'status');
        if( $status && $status !='ALL')
        {
            $this->db->like( [ 'company.status' => $status ] );
        }

    }
        
    public function getAvailableStatus()
    {
        //#translate text?
        return $this->available_status;
    }

    public function getCountStatus()
    {
        $this->db->select([
            'count(status) as total_status',
            'status'
        ])
        ->from('company')
        ->group_by('status');

        return $this->db->get()->result_array();
    }

    public function getCompanyByName($name){
        $this->db->select("* ")
                 ->from('company');
            if($name!=''){
            $this->db->like('name', $name);
            }
            $this->db->limit(10, 0);
        return $this->db->get()->result_array();

    }
    
}
