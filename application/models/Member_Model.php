<?php

class Member_Model extends APP_Model
{
    private $basicFields = [
            'user.id',
            'user.email',
            'user.status',
            'user.names',
            'user.role',
        ];


    public function getProyect($id)
    {
            $basic=$this->basicFields;
            $basic[5]='member.id as member_id';
        $this->db->select($basic)
                     ->from("user")
                     ->join("member","member.user_id=user.id","inner")
                     ->where("member.project_id",$id);
        return $this->db->get()->result_array();

    }

    public function otherUser($users)
    {
        $this->db->select($this->basicFields)
                     ->from("user");
            foreach ($users as &$value) {
                $this->db->where("id!=",$value['id']);  
            }
        return $this->db->get()->result_array();
    }

    
}
