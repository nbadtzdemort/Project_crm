<?php

class User_Model extends APP_Model
{
	public function getSearch( $limit,  $start, $data = null ) 
    {
    	$fieldsSort = [
    		'id' => 'user.id',
            'email' => 'user.email',
            'status' => 'user.status',
            'names' => 'user.names',
            'role' => 'user.role',
    	];

    	$basicFields = [
			'user.id',
            'user.email',
            'user.status',
            'user.names',
            'user.role',
		];

		$where = isset( $data['filters'] ) ?  $data['filters'] : null;
 
		$this->_filter( $where ); 

        $this->db->select( $basicFields )
        	->from('user');

    	
       	$this->db->limit(abs($limit),abs($start) );

        $sortName = isset( $data['sort']['name']) ? $data['sort']['name'] : '';
        $sortType = isset( $data['sort']['type']) ? $data['sort']['type'] : '';

        
        if( isset($fieldsSort[$sortName]) && in_array($sortType, ['asc','desc']) )
        {	
             
			$this->db->order_by($fieldsSort[$sortName].' '.$data['sort']['type']);
        }
        else
        {
        	$this->db->order_by('user.id');
        }
       	
        $result_data = $this->db->get()->result();    	
    	
    	$this->_filter( $where );

        $this->db->from('user');

        $total_count = $this->db->count_all_results();	
    	
    	return [
    		'total_count' => $total_count,
    		'result_data' => $result_data
    	];
    }

    private function _filter( $where = null ) 
    {
        if($email = $this->filterValue($where,'email'))
        {
            
            $this->db->like( [ 'email' => $email ]  );
        }
        if($names = $this->filterValue($where,'names'))
        {
            $this->db->like( [ 'names' => $names ] );
        }
        
        $role = $this->filterValue($where,'role');
        if( $role && $role !='ALL')
        {
            $this->db->like( [ 'role' => $role ] );
        }
    }

}
