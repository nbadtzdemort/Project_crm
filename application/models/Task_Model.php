<?php

class Task_Model extends APP_Model
{
    private $basicFields = [
            'task.id', 
            'usercreated.names as created',
            'task.project_id', 
            'task.nota', 
            'task.create_at', 
            'task.start_date', 
            'task.end_date', 
            'task.status', 
            'userasigned.names as asigned' 
        ];


    public function getTask($id)
    {
        $this->db->select($this->basicFields)
                     ->from("task")
                     ->join("user as usercreated","task.user_created=usercreated.id","inner")
                     ->join("user as userasigned","task.user_asigned=userasigned.id","inner")
                     ->where("project_id",$id);
        return $this->db->get()->result_array();
    }

    public function getSearch( $limit,  $start, $data = null, $id ) 
    {   
        $fieldsSort = [
            'id'=>'task.id', 
            'created'=>'usercreated.names as created',  
            'project_id'=>'task.project_id', 
            'nota'=>'task.nota', 
            'create_at'=>'task.create_at', 
            'start_date'=>'task.start_date', 
            'end_date'=>'task.end_date', 
            'status'=>'task.status',
            'name'=>'project.name'
        ];

        $fields=$this->basicFields;
        $fields[8]='project.name';
        $where = isset( $data['filters'] ) ?  $data['filters'] : null;
        
        $this->_filter( $where ); 

        $this->db->select($fields)
            ->from('task')
            ->join("user as usercreated","task.user_created=usercreated.id","inner")
            ->join("project","task.project_id=project.id","inner")
            ->where('user_asigned',$id);
        $this->db->limit(abs($limit),abs($start) );

        $sortName = isset( $data['sort']['name']) ? $data['sort']['name'] : '';
        $sortType = isset( $data['sort']['type']) ? $data['sort']['type'] : '';

        
        if( isset($fieldsSort[$sortName]) && in_array($sortType, ['asc','desc']) )
        {   
             
            $this->db->order_by($fieldsSort[$sortName].' '.$data['sort']['type']);
        }
        else
        {
            $this->db->order_by('task.create_at');
        }
        
        $result_data = $this->db->get()->result();      
        
        $this->_filter( $where );

        $this->db->from('task')
        ->join("user as usercreated","task.user_created=usercreated.id","inner")
        ->join("project","task.project_id=project.id","inner")
        ->where('user_asigned',$id);

        $total_count = $this->db->count_all_results();

        return ['total_count' => $total_count,'result_data' => $result_data];
    }

    private function _filter( $where = null ) 
    {
        if($project_name = $this->filterValue($where,'project_name'))
        {
            $this->db->like( [ 'name' => $project_name ]  );
        }
        if($nota = $this->filterValue($where,'nota'))
        {
            $this->db->like( [ 'nota' => $nota ] );
        }
        if($create_at = $this->filterValue($where,'created_at'))
        {
            $this->db->like( [ 'create_at' => $create_at ] );
        }
        if($start_date = $this->filterValue($where,'start_date'))
        {
            $this->db->like( [ 'start_date' => $start_date ]  );
        }
        if($end_date = $this->filterValue($where,'end_date'))
        {
            $this->db->like( [ 'task.end_date' => $end_date ] );
        }
        if($names = $this->filterValue($where,'user_created'))
        {
            $this->db->like( [ 'usercreated.names' => $names ] );
        }
    }

}

 