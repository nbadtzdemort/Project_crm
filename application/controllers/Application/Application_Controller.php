<?php
/**
* @route:application
*/
class Application_Controller extends APP_Controller
{
	/**
	 * @route:__avoid__
	 */
	function index(){

		$this->template->layout('admin');

		$current_user = $this->current_user;


		$this->template->body([
			'ng-controller' => 'MainCtrl as main',
			'ng-init' => '$state.current_user='.$this->template->json_entities($this->current_user),
			'id' => 'page-top'
		]);
		
		$this->template->js('', base_url('app/application.js') );
		$this->template->js('', base_url('app/states.js') );
		$this->template->js('', base_url('app/factory.js') );
		$this->template->js('', base_url('app/directives.js') );
		
		$this->_loadControllerJS();
		
		$this->template->render('application', [
			'User_DB' => $this->current_user,
			'total_users' => $this->User_DB->getCount()
		]);

	}

	/**
	 * @rotue{get}init
	 */
	public function init()
	{

		$this->load->model([
			'Company_Model' => 'Company_DB',
			'Company_Communication_Model' => 'Company_Communication_DB'
		]);

		$status_counter   = $this->Company_DB->getCountStatus();
		$status_companies = $this->Company_DB->getAvailableStatus();
		
		$chart_company_data =  $chart_company_labels = [];
			
		foreach ($status_counter as $key => $result) {
			$chart_company_data[]   = $result['total_status'];
			$chart_company_labels[] = $status_companies[$result['status']];
		}
		
		$this->template->json([
			'company_communications' => $this->Company_Communication_DB->getLast(5),
			'chart_company' => [
				'data' => $chart_company_data,
				'labels' => $chart_company_labels
			]
		]);
		
	}

	private function _loadControllerJS()
	{	

		$files = new RecursiveIteratorIterator( 
            new RecursiveDirectoryIterator( APPPATH.'..'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR)
        );
        
        foreach ($files as $file)
        {
            $file_info = pathinfo( $file );
             
            if(!isset($file_info['extension']) || $file_info['extension']!='js')
            {
                continue;
            }
          	
			$file_out = str_replace(__DIR__.''.DIRECTORY_SEPARATOR, '' , $file );
			$F        = str_replace(APPPATH."..".DIRECTORY_SEPARATOR."public",'',$file_out);
			
            $this->template->js('', base_url(str_replace(DIRECTORY_SEPARATOR, "/",$F) ) );
        	
        }
	}
}
