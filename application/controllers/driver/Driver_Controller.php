<?php
/**
* @route:driver
*/
class Driver_Controller extends APP_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(['Driver_Model' => 'Driver_DB'
        ]);
    }

    /**
     * @route:{get}search/(:num)/(:num)
     */
    public function search( $maxRecords = 0, $page = 0)
    {
        
        $limit = ($maxRecords==0 || $maxRecords>100) ? 10 : $maxRecords;
        $start = ( $limit * $page ) - $limit;
        
        $this->template->json( $this->Driver_DB->getSearch($limit, $start, $this->input->get()  ) );
    }

    /**
     * @route:{post}create
     */
    public function create()
    {

        $this->form_validation
            ->set_rules('name','Names','trim|required|is_unique[driver.names]')
            ->set_rules('cargo','Cargo','trim|required');

        if( $this->form_validation->run() === FALSE )
        {   
            $response['message'] = $this->form_validation->error_string(); 
        }
        else
        {
            
            $this->Driver_DB->names   = $this->input->post('name');
            $this->Driver_DB->cargo  = $this->input->post('cargo');
            $this->Driver_DB->status = 1;
            $id                   = $this->Driver_DB->save();
            $response = [
                'status' => 1,
                'message' => 'Driver created',
                'driver' => $this->Driver_DB->get( $id ),
                'total_driver' => $this->Driver_DB->getCount()
            ];
            
        }
        
        $this->template->json($response);
    }

    /**
     * @route:{get}(:num)/info
     */
    public function info( $id )
    {
        $driver = $this->Driver_DB->recordRequired( $id);
        $this->template->json([
            'driver' => $driver
        ]);
    }

    /**
     * @route:{post}(:num)/update
     */
    public function update( $id = 0)
    {
        $this->Driver_DB->recordRequired( $id);

        $this->form_validation
            ->set_rules('name','Name','trim|required|is_unique[driver.name]')
            ->set_rules('cargo','Cargo','trim|required');
        ;
        
        if( $this->form_validation->run() === FALSE )
        {   
            $response['message'] = $this->form_validation->error_string(); 
            
        }
        else
        {   
            $this->Driver_DB->name        = $this->input->post('name');
            $this->Driver_DB->cargo  = $this->input->post('cargo');
            
            $response = [
                'status' => 1,
                'message' => 'Driver updated',
                'driver' => $this->Driver_DB->save( $id )
            ];
            
        }
        
        $this->template->json($response);
    }
}