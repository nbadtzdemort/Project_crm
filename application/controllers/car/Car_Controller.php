<?php
/**
* @route:car
*/
class Car_Controller extends APP_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(['Car_Model' => 'Car_DB',
                            'Driver_Model' => 'Driver_DB',
                            'Control_Model' => 'Control_DB',
        ]);
    }

    /**
     * @route:{get}search/(:num)/(:num)
     */
    public function search( $maxRecords = 0, $page = 0)
    {
        
        $limit = ($maxRecords==0 || $maxRecords>100) ? 10 : $maxRecords;
        $start = ( $limit * $page ) - $limit;
        
        $this->template->json( $this->Car_DB->getSearch($limit, $start, $this->input->get()  ) );
    }

    /**
     * @route:{post}create
     */
    public function create()
    {

        $this->form_validation
            ->set_rules('name','Name','trim|required|is_unique[car.name]')
            ->set_rules('model','Model','trim|required');

        if( $this->form_validation->run() === FALSE )
        {   
            $response['message'] = $this->form_validation->error_string(); 
        }
        else
        {
            
            $this->Car_DB->name   = $this->input->post('name');
            $this->Car_DB->model  = $this->input->post('model');
            $this->Car_DB->status = 1;
            $id                   = $this->Car_DB->save();
            $response = [
                'status' => 1,
                'message' => 'Car created',
                'car' => $this->Car_DB->get( $id ),
                'total_car' => $this->Car_DB->getCount()
            ];
            
        }
        
        $this->template->json($response);
    }

    /**
     * @route:{get}(:num)/info
     */
    public function info( $id )
    {
        $car = $this->Car_DB->recordRequired( $id);
        $this->template->json([
            'car' => $car
        ]);
    }

    /**
     * @route:{get}controlInfo
     */
    public function controlInfo()
    {
        $car = $this->Car_DB->getAll();
        $driver = $this->Driver_DB->getAll();
        $control = $this->Control_DB->getAll();
        $location = $this->Control_DB->getAllLocation();
        $this->template->json([
            'car' => $car,
            'driver' => $driver,
            'control' => $control,
            'location' => $location
        ]);
    }

    /**
     * @route:{post}(:num)/update
     */
    public function update( $id = 0)
    {
        $this->Car_DB->recordRequired( $id);

        $this->form_validation
            ->set_rules('name','Name','trim|required|is_unique[car.name]')
            ->set_rules('model','Model','trim|required');
        ;
        
        if( $this->form_validation->run() === FALSE )
        {   
            $response['message'] = $this->form_validation->error_string(); 
            
        }
        else
        {   
            $this->Car_DB->name        = $this->input->post('name');
            $this->Car_DB->model  = $this->input->post('model');
            
            $response = [
                'status' => 1,
                'message' => 'Project updated',
                'project' => $this->Car_DB->save( $id )
            ];
            
        }
        
        $this->template->json($response);
    }

    /**
     * @route:{post}newControl
     */
    public function newControl()
    {
        $this->form_validation
            ->set_rules('name_car','Car','trim|required')
            ->set_rules('names','Driver','trim|required')
            ->set_rules('date','Date','trim|required')
            ->set_rules('location','Location','trim|required');

        if( $this->form_validation->run() === FALSE )
        {   
            $response['message'] = $this->form_validation->error_string(); 
        }
        else
        {
            $this->Control_DB->car      = $this->input->post('name_car');
            $this->Control_DB->driver   = $this->input->post('names');
            $this->Control_DB->date     = date_format(date_create($this->input->post('date')),"Y-m-d");
            $this->Control_DB->location = $this->input->post('location');
            $id                         = $this->Control_DB->save();
            $location                   = $this->Control_DB->getLocationByName($this->input->post('location'));
            $response = [
                'status' => 1,
                'message' => 'Control created',
                'control'=> $this->Control_DB->get($id),
                'messageLocaton'=>$location
            ];
            
        }
        
        $this->template->json($response);
    }
}