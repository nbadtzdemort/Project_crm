<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @route:login
 */
class Login_Controller extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library( ['install/Install'  => 'LibInstall'] );
		
		

		$this->LibInstall->check();
	}
	
	/** 
	 * @route:__avoid__
	 */
	public function index()
	{
		if(isset($this->session->userdata['User_DB']) and isset($this->session->userdata['User_DB']->id))
		{	
			redirect('/application/');
		}
		
		$email = $this->input->get('email');
		
		$this->template
			->layout('public')
			->render('login/view_form', [
				'email' => $email
			]);

	}

	/**
	 * @route:intent
	 */
	function intent()
	{
		
		$user = $this->User_DB->getRowBy([
			'email' => $this->input->post('email')
		]);

		if( !$user )
		{	
			$this->template->setNotify('error' ,'User or password invalid' );
		}
		else if( (int)$user->status === 0 )
		{	
			$this->template->setNotify('error' ,'User disabled, please contact admin' );
		}
		else if(!password_verify( $this->input->post('password') ,  $user->password   ) )
		{
			$this->template->setNotify('error' ,'User or password invalid' );
		}
		else
		{	
			$this->template->setNotify('success' ,'Welcome :)' );
			
			unset($user->password);
			
			$this->session->set_userdata(['User_DB' => $user ]);
			
			redirect('/application/');	
		}
		
		
		$Data = [
			'email' => $this->input->post('email')
		];
		
		redirect('/login/?'. http_build_query( $Data));
	}

	/**
	 * @route:close
	 */
	function close()
	{
		$this->session->unset_userdata('User_DB');
		$this->template->setNotify('success', 'End session');
        redirect('/');
	}
}
