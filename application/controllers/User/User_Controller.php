<?php
/**
* @route:user
*/
class User_Controller extends APP_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * @route:{get}search/(:num)/(:num)
	 */
	public function search( $maxRecords = 0, $page = 0)
	{
		
		$limit = ($maxRecords==0 || $maxRecords>100) ? 10 : $maxRecords;
		$start = ( $limit * $page ) - $limit;
		
		$this->template->json( $this->User_DB->getSearch($limit, $start, $this->input->get()  ) );
	}

	/**
	 * @route:{post}create
	 */
	public function create()
	{
		
		$this->form_validation
			->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[user.email]')
			->set_rules('password', 'Password', 'trim|required|min_length[4]')
			->set_rules('names','Names','trim|required|min_length[6]')
			->set_rules('role','Full name','required|in_list[admin,collaborator]')
		;
		
		if( $this->form_validation->run() === FALSE )
		{	
			$response['message'] = $this->form_validation->error_string(); 
		}
		else
		{
			
			$this->User_DB->email    = strtolower( $this->input->post('email') );
			$this->User_DB->password = password_hash($this->input->post('password') , PASSWORD_BCRYPT );
			$this->User_DB->names    = $this->input->post('names');
			$this->User_DB->role     = $this->input->post('role');
			$this->User_DB->status   = 1;
			$id = $this->User_DB->save();

			$response = [
				'status' => 1,
				'message' => 'User created',
				'user' => $this->User_DB->get( $id ),
				'total_users' => $this->User_DB->getCount()
			];
			
		}
		
		$this->template->json($response);
	}

	/**
	 * @route:{get}(:num)/info
	 */
	public function info( $id )
	{
		$user = $this->User_DB->recordRequired( $id);
		
		$this->template->json([
			'user' => $user
		]);
	}
	
	/**
	 * @route:{post}(:num)/update
	 */
	public function update( $id = 0)
	{
		$this->User_DB->recordRequired( $id);

		$this->form_validation
			->set_rules('names','Names','trim|required|min_length[6]')
			->set_rules('role','Full name','required|in_list[admin,collaborator]')
			->set_rules('status','Status','required|in_list[0,1]')
		;
		
		if( $this->form_validation->run() === FALSE )
		{	
			$response['message'] = $this->form_validation->error_string(); 
			
		}
		else
		{	
			$this->User_DB->names  = $this->input->post('names');
			$this->User_DB->role   = $this->input->post('role');
			$this->User_DB->status = $this->input->post('status');
			
			$response = [
				'status' => 1,
				'message' => 'User updated',
				'user' => $this->User_DB->save( $id )
			];
			
		}
		
		$this->template->json($response);
	}

	/**
	 * @route:{delete}(:num)/delete
	 */
	public function delete( $id = 0)
	{
		$this->User_DB->recordRequired( $id);
		
		$this->template->json(['message' => '**DELETE**']);
	}

}
