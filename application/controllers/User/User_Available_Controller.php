<?php
/**
* @route:user-available
*/
class User_Available_Controller extends APP_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	
	/**
	 * @route:{get}all
	 */
	public function all( $id = 0)
	{
		$response = [
			'status' => 1,
			'user_list' => $this->User_DB->getAll(['status' => 1]),
			'current_user' => $this->current_user
		];
		
		$this->template->json($response);
	}

}
