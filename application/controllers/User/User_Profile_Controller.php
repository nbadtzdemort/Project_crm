<?php
/**
* @route:user/profile
*/
class User_Profile_Controller extends APP_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * @route:{get}info
	 */
	public function info()
	{	
		$this->template->json([
			'user' => $this->current_user
		]);	
	}
	
	/**
	 * @route:{post}update
	 */
	public function update( $id = 0)
	{
		$this->User_DB->recordRequired( $id);

		$this->form_validation
			->set_rules('names','Names','trim|required|min_length[6]')
			->set_rules('role','Full name','required|in_list[admin,collaborator]')
			->set_rules('status','Status','required|in_list[0,1]')
		;
		
		if( $this->form_validation->run() === FALSE )
		{	
			$response['message'] = $this->form_validation->error_string(); 
			
		}
		else
		{	
			$this->User_DB->names  = $this->input->post('names');
			$this->User_DB->role   = $this->input->post('role');
			$this->User_DB->status = $this->input->post('status');
			
			$response = [
				'status' => 1,
				'message' => 'User updated',
				'user' => $this->User_DB->save( $id )
			];
			
		}
		
		$this->template->json($response);
	}

	/**
	 * @route:{post}change-password
	 */
	public function change_password( $id = 0)
	{
		$this->User_DB->recordRequired( $id);

		$this->form_validation
			->set_rules('names','Names','trim|required|min_length[6]')
			->set_rules('role','Full name','required|in_list[admin,collaborator]')
			->set_rules('status','Status','required|in_list[0,1]')
		;
		
		if( $this->form_validation->run() === FALSE )
		{	
			$response['message'] = $this->form_validation->error_string(); 
			
		}
		else
		{	
			$this->User_DB->names  = $this->input->post('names');
			$this->User_DB->role   = $this->input->post('role');
			$this->User_DB->status = $this->input->post('status');
			
			$response = [
				'status' => 1,
				'message' => 'User updated',
				'user' => $this->User_DB->save( $id )
			];
			
		}
		
		$this->template->json($response);
	}

}
