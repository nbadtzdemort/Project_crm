<?php
/**
* @route:project
*/
class Project_Controller extends APP_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model(['Project_Model' => 'Project_DB',
			'Member_Model' => 'Member_DB',
			'Task_Model' => 'Task_DB',
			'Company_Model' => 'Company_DB',
			'Log_Model' => 'Log_DB'
		]);
	}

	/**
	 * @route:{get}search/(:num)/(:num)
	 */
	public function search( $maxRecords = 0, $page = 0)
	{
		
		$limit = ($maxRecords==0 || $maxRecords>100) ? 10 : $maxRecords;
		$start = ( $limit * $page ) - $limit;
		
		$this->template->json( $this->Project_DB->getSearch($limit, $start, $this->input->get()  ) );
	}

	/**
	 * @route:{post}create
	 */
	public function create()
	{

		$this->form_validation
			->set_rules('name','Name','trim|required')
			->set_rules('company','Company','trim|required')
			->set_rules('end_date','End Date','trim|required|valid_date|date_min_today')
			->set_rules('description','Description','trim|required|min_length[10]|max_length[5000]');

		if( $this->form_validation->run() === FALSE )
		{	
			$response['message'] = $this->form_validation->error_string(); 
		}
		else
		{
			
			$this->Project_DB->name        = $this->input->post('name');
			$this->Project_DB->company_id  = $this->input->post('company');
			$this->Project_DB->create_date = date("Y-m-d");
			$this->Project_DB->end_date    = date_format(date_create($this->input->post('end_date')),"Y-m-d");
			$this->Project_DB->description = $this->input->post('description');
			$id                            = $this->Project_DB->save();
			$this->Log_DB->log("Project_Controller->create","Project -".$this->input->post('name')."- created", $this->current_user->id,2);
			$response = [
				'status' => 1,
				'message' => 'Project created',
				'project' => $this->Project_DB->get( $id ),
				'total_project' => $this->Project_DB->getCount()
			];
			
		}
		
		$this->template->json($response);
	}

/**
	 * @route:{get}(:num)/info
	 */
	public function info( $id )
	{
		$project = $this->Project_DB->recordRequired( $id);
		$user = $this->Member_DB->getProyect($id);
		$otherUser= $this->Member_DB->otherUser($user);
		$task= $this->Task_DB->getTask($id);
		$project->end_date=date_format(date_create($project->end_date),"m/d/Y");
		$project->create_date=date_format(date_create($project->create_date),"m/d/Y");
		$this->template->json([
			'project' => $project,
			'member'  => $user,
			'user'    => $otherUser,
			'task'    => $task,
			'company' => $this->Company_DB->get($project->company_id)
		]);
		
		
	}
	
	/**
	 * @route:{post}(:num)/update
	 */
	public function update( $id = 0)
	{
		$project_data=$this->Project_DB->recordRequired( $id);

		$this->form_validation
			->set_rules('name','Name','trim|required')
			->set_rules('company_name','Company','trim|required')
			->set_rules('end_date','End_Date','trim|required')
			->set_rules('description','Description','trim|required');
		;
		
		if( $this->form_validation->run() === FALSE )
		{	
			$response['message'] = $this->form_validation->error_string(); 
			
		}
		else
		{	
			$this->Project_DB->name        = $this->input->post('name');
			$this->Project_DB->company_id  = $this->input->post('company_name');
			$this->Project_DB->create_date = date("Y-m-d");
			$this->Project_DB->end_date    = date_format(date_create($this->input->post('end_date')),"Y-m-d");
			$this->Project_DB->description = $this->input->post('description');
			$mensaje="Project update ";
			if($project_data->name!=$this->input->post('name')){$mensaje.="(".$project_data->name.")";}
			if($project_data->company_id!=$this->input->post('company_name'))
			{$mensaje.="(".$this->Company_DB->get($project_data->company_id)->name.")";}
			if($project_data->end_date!=date_format(date_create($this->input->post('end_date')),"Y-m-d"))
			{$mensaje.="(".$project_data->end_date.")";}
			if($project_data->description!=$this->input->post('description'))
			{$mensaje.="(".$project_data->description.")";}
			$this->Log_DB->log("Project_Controller->update",$mensaje, $this->current_user->id,1);
			$response = [
				'status' => 1,
				'message' => 'Project updated',
				'project' => $this->Project_DB->save( $id )
			];
			
		}
		
		$this->template->json($response);
	}

	/**
	 * @route:{post}(:num)/task
	 */
	public function createTask( $id )
	{	
		$this->Project_DB->recordRequired( $id);
		$this->form_validation
			->set_rules('name','Name','trim|required')
			->set_rules('user_id','User','trim|required')
			->set_rules('start_date','Start Date','trim|required|valid_date|date_min_today')
			->set_rules('end_date','End Date','trim|required|valid_date|date_min_today');

		if( $this->form_validation->run() === FALSE )
		{	
			$response['message'] = $this->form_validation->error_string(); 
			
		}
		else
		{	
			$this->Task_DB->project_id   = $id;
			$this->Task_DB->nota         = $this->input->post('name');
			$this->Task_DB->create_at    = date("Y-m-d H:i:s");
			$this->Task_DB->start_date   = date_format(date_create($this->input->post('start_date')),"Y-m-d");
			$this->Task_DB->end_date     = date_format(date_create($this->input->post('end_date')),"Y-m-d");
			$this->Task_DB->status       = 0;
			$this->Task_DB->user_created  = $this->current_user->id;
			$this->Task_DB->user_asigned = $this->input->post('user_id');
			$id_task                        = $this->Task_DB->save();
			$this->Log_DB->log("Project_Controller->createTask","Task Created", $this->current_user->id,2);
			$response = [
				'status'  => 1,
				'message' => 'Task Asigned',
				'tasks'    => $this->Task_DB->getTask($id)
			];
			
		}

		$this->template->json($response);
	}

	/**
	 * @route:{post}updateTask
	 */
	public function updateTask( )
	{	
		$project_data=$this->Task_DB->recordRequired($this->input->post('task'));
			$this->Task_DB->status       = $this->input->post('val');
			$this->Task_DB->save( $this->input->post('task') );
			if($this->input->post('val')==2){$mensaje="Task Complete";}
			if($this->input->post('val')==1){$mensaje="Task In Progress";}
			$this->Log_DB->log("Project_Controller->update",$mensaje, $this->current_user->id,1);
			$response = [
				'status' => 1,
				'message' => $mensaje,
				'tasks' => $this->Task_DB->getTask($this->current_user->id)
			];
	
		$this->template->json($response);
	}

/**
	 * @route:{get}searchTask/(:num)/(:num)
	 */
	public function searchTask( $maxRecords = 0, $page = 0)
	{
		
		$limit = ($maxRecords==0 || $maxRecords>14) ? 14 : $maxRecords;
		$start = ( $limit * $page ) - $limit;
		
		$this->template->json( $this->Task_DB->getSearch($limit, $start, $this->input->get(), $this->current_user->id ) );
	}

}
