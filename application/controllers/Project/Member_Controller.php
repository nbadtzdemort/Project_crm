<?php
/**
* @route:member
*/
class Member_Controller extends APP_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model([
			'Member_Model' => 'Member_DB',
			'Project_Model' => 'Project_DB',
			'User_Model' => 'User_DB',
			'Log_Model' => 'Log_DB'
		]);
	}

	/**
	 * @route:{post}(:num)/add
	 */
	public function add($project_id)
	{
		$project = $this->Project_DB->recordRequired($project_id);
		$this->form_validation
			->set_rules('user_id','Member','trim|required')
			->set_rules('project_id','Project','trim|required');

		if( $this->form_validation->run() === FALSE )
		{	
			$response['message'] = $this->form_validation->error_string(); 
		}
		else
		{
			$this->Member_DB->user_id    = $this->input->post('user_id');
			$this->Member_DB->project_id = $this->input->post('project_id');
			$id                               = $this->Member_DB->save();
			$this->Log_DB->log("Member_Controller->add","User ".$this->User_DB->get($this->input->post('user_id'))->names." added in ".$project->name, $this->current_user->id,2);
			$response = [
				'status'  => 1,
				'message' => 'Miembro Agregado',
				'members' => $this->Member_DB->getProyect($project_id),
				'user_add'    => $this->Member_DB->otherUser($this->Member_DB->getProyect($project_id))
			];
		
		}

		$this->template->json($response);

	}

	/**
	 * @route:{delete}(:num)/delete
	 */
	public function delete( $id )
	{
		$member = $this->Member_DB->recordRequired($id);

		$this->Member_DB->delete($id);
		$this->Log_DB->log("Member_Controller->delete","User ".$this->User_DB->get($member->user_id)->names." deleted in ".$this->Project_DB->get($member->project_id)->name, $this->current_user->id,3);
		$this->template->json(['status' => 1, 
							   'message'=>"Member Delete"]);
	}
	
}
