<?php
/**
* @route:company/communication
*/
class Company_Communication_Controller extends APP_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$this->load->model([
			'Company_Model' => 'Company_DB',
			'Company_Communication_Model' => 'Company_Communication_DB'
		]);
	}

	/**
	 * @route:{post}(:num)/save
	 */
	public function save( $company_id )
	{
		$company = $this->Company_DB->recordRequired( $company_id);

		$this->form_validation
			->set_rules('type', 'Type', 'trim|required|in_list[1,2,3]')
			->set_rules('message','Message','trim|required|min_length[10]|max_length[5000]')
		;
		
		if( $this->form_validation->run() === FALSE )
		{	
			$response['message'] = $this->form_validation->error_string(); 
		}
		else
		{
			
			$this->Company_Communication_DB->company_id = $company_id;
			$this->Company_Communication_DB->type       = $this->input->post('type');
			$this->Company_Communication_DB->message    = $this->input->post('message');
			$this->Company_Communication_DB->created_at = date('Y-m-d H:i:s');
			$this->Company_Communication_DB->user_id    = $this->current_user->id;
			
			$id = $this->Company_Communication_DB->save();
			
			$response = [
				'status' => 1,
				'message' => 'Communication was created',
				'communication' => $this->Company_Communication_DB->getDetail($id)
			];
		}
		
		$this->template->json($response);
	}
}