<?php
/**
* @route:company/contact-list
*/
class Contact_List_Controller extends APP_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$this->load->model([
			'Company_Model' => 'Company_DB',
			'Contact_List_Model' => 'Contact_List_DB'
		]);
	}

	/**
	 * @route:{post}(:num)/save
	 */
	public function save( $company_id )
	{
		$company = $this->Company_DB->recordRequired( $company_id);

		$this->form_validation
			->set_rules('names', 'Name', 'trim|required|max_length[120]')
			->set_rules('email','Email','trim|valid_email|max_length[120]')
			->set_rules('phone','Phone','trim|required|max_length[10]')
			->set_rules('comments','Commments','trim|max_length[5000]')
		;
		
		if( $this->form_validation->run() === FALSE )
		{	
			$response['message'] = $this->form_validation->error_string(); 
		}
		else
		{
			
			$this->Contact_List_DB->company_id = $company_id;
			$this->Contact_List_DB->names      = $this->input->post('names');
			$this->Contact_List_DB->email      = strtolower( $this->input->post('email') );
			$this->Contact_List_DB->phone      = $this->input->post('phone');
			$this->Contact_List_DB->comments   = $this->input->post('comments');
			$this->Contact_List_DB->default    = 0;
			$this->Contact_List_DB->updated_at = date('Y-m-d H:i:s');
			

			if($contact_list_id = $this->input->post('id'))
			{
				$contact    = $this->Contact_List_DB->recordRequired( $contact_list_id);
				$this->Contact_List_DB->save( $contact_list_id );
				$message = "Contact was updated";
			}
			else
			{
				$contact_list_id = $this->Contact_List_DB->save();
				$message = "Contact was created";
			}
				
			if($this->input->post('default'))
			{
				$this->Contact_List_DB->changeDefault( $company_id , $contact_list_id );
			}
			
			$response = [
				'status' => 1,
				'message' => $message,
				'contact_list' => $this->Contact_List_DB->getAll(['company_id' => $company_id ])
			];
		}
		
		$this->template->json($response);
	}


	/**
	 * @route:{delete}(:num)/delete
	 */
	public function delete( $id )
	{
		$this->Contact_List_DB->recordRequired( $id);
		
		$this->Contact_List_DB->delete($id);

		$this->template->json(['status' => 1]);
	}
}