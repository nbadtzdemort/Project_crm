<?php
/**
* @route:company
*/
class Company_Controller extends APP_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$this->load->model([
			'Company_Model' => 'Company_DB',
			'Contact_List_Model' => 'Contact_List_DB'
		]);
	}
	
	/**
	 * @route:{get}search/(:num)/(:num)
	 */
	public function search( $maxRecords = 0, $page = 0)
	{
		
		$limit = ($maxRecords==0 || $maxRecords>100) ? 10 : $maxRecords;
		$start = ( $limit * $page ) - $limit;
		
		$this->template->json( $this->Company_DB->getSearch($limit, $start, $this->input->get()  ) );
	}

	/**
	 * @route:{get}search-initial
	 */
	public function search_initial()
	{
		$this->template->json([
			'status_company' => $this->Company_DB->getAvailableStatus()
		]);
	}
	
	/**
	 * @route:{post}create
	 */
	public function create()
	{
		
		$this->form_validation
			->set_rules('name', 'Name', 'trim|required|max_length[250]')
			->set_rules('address','Address','trim|required|max_length[500]')
			->set_rules('source','Source','trim|required|max_length[75]')
			->set_rules('contact_name','Contact name','trim|required|max_length[150]')
			->set_rules('contact_phone','Contact phone','trim|required|max_length[10]')
			->set_rules('contact_email','Email','trim|max_length[75]')
			->set_rules('user_owner','Owner','required|exist_data[user.id]')
		;
		
		if( $this->form_validation->run() === FALSE )
		{	
			$response['message'] = $this->form_validation->error_string(); 
		}
		else
		{
			
			$this->Company_DB->name          = $this->input->post('name');
			$this->Company_DB->address       = $this->input->post('address');
			$this->Company_DB->source        = $this->input->post('source');
			$this->Company_DB->user_owner    = $this->input->post('user_owner');
			
			$this->Company_DB->contact_name  = $this->input->post('contact_name');
			$this->Company_DB->contact_phone = $this->input->post('contact_phone');
			$this->Company_DB->contact_email = $this->input->post('contact_email');
			
			$this->Company_DB->status        = 1;
			$id = $this->Company_DB->save();
			
			$this->Contact_List_DB->company_id = $id;
			$this->Contact_List_DB->names      = $this->input->post('contact_name');
			$this->Contact_List_DB->phone      = $this->input->post('contact_phone');
			$this->Contact_List_DB->email      = $this->input->post('contact_email');
			$this->Contact_List_DB->save();

			$response = [
				'status' => 1,
				'message' => 'Company created',
				'company' => $this->Company_DB->get( $id )
			];
			
		}
		
		$this->template->json($response);
	}

	/**
	 * @route:{post}(:num)/update
	 */
	public function update( $id )
	{

		$company = $this->Company_DB->recordRequired( $id );

		$this->form_validation
			->set_rules('name', 'Name', 'trim|required|max_length[250]')
			->set_rules('address','Address','trim|required|max_length[500]')
			->set_rules('source','Source','trim|required|max_length[75]')
			->set_rules('status','Status','trim|required|in_list[1,2,3]')
			->set_rules('user_owner','Owner','required|exist_data[user.id]')
		;
		
		if( $this->form_validation->run() === FALSE )
		{	
			$response['message'] = $this->form_validation->error_string(); 
		}
		else
		{
			
			$this->Company_DB->name          = $this->input->post('name');
			$this->Company_DB->address       = $this->input->post('address');
			$this->Company_DB->source        = $this->input->post('source');
			$this->Company_DB->status        = $this->input->post('status');
			$this->Company_DB->user_owner    = $this->input->post('user_owner');
			$this->Company_DB->save( $id );
			
			$response = [
				'status' => 1,
				'message' => 'Company was updated',
			];
			
		}
		
		$this->template->json($response);
	}
	
	/**
	 * @route:{get}(:num)/info
	 */
	public function info( $id )
	{

		$this->load->model([
			'Company_Communication_Model' => 'Company_Communication_DB',
		]);
		
		$company          = $this->Company_DB->recordRequired( $id);
		$contact_list 	  = $this->Contact_List_DB->getAll(['company_id' => $id ]);
		$communications   = $this->Company_Communication_DB->getAllByCompany($id);
		
		$this->template->json([
			'company' => $company,
			'contact_list' => $contact_list,
			'communications' => $communications,
			'types_communication' => $this->Company_Communication_DB->getAvailableStatus(),
			'user_list' => $this->User_DB->getAll(),
			'status_company' => $this->Company_DB->getAvailableStatus()
		]);
	}

	/**
	 * @route:{get}(:any)/infoCompany
	 */
	public function infoCompany( $name )
	{
		$name=str_replace("%20"," ",$name);
		$company = $this->Company_DB->getCompanyByName($name);
		$response = ["company"=>$company];
		$this->template->json($response);
	}


}