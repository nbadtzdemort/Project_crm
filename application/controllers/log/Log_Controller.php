<?php
/**
* @route:log
*/
class Log_Controller extends APP_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(['Log_Model' => 'Log_DB',
                            'User_Model' => 'User_DB',
                            'Project_Model' => 'Control_DB',
        ]);
    }

    /**
     * @route:{get}myLogs
     */
    public function myLogs()
    {
        $logs=$this->Log_DB->log_list("","",$this->current_user->id);
        $response = [
                'status' => 1,
                'logs' => $logs,
                'type' => 0
            ];
        $this->template->json($response);
    }

    /**
     * @route:{get}allLogs
     */
    public function allLogs()
    {
        $logs=$this->Log_DB->log_list("","",0);
        $response = [
                'status' => 1,
                'logs' => $logs,
                'type' => 1
            ];
        $this->template->json($response);
    }

    /**
     * @route:{post}LogsSearch
     */
    public function LogsSearch()
    {
        $id=0;
        if($this->input->post('user')==0){
            $id=$this->current_user->id;
        }
        if($this->input->post('search')=='date'){
            $value=date_format(date_create($this->input->post('value')),"Y-m-d");
        }else{
            $value=$this->input->post('value');
        }

        $logs=$this->Log_DB->log_list($this->input->post('search'),$value,$id);
        $response = [
                'status' => 1,
                'logs' => $logs,
                'type' => 1
            ];
        $this->template->json($response);
    }
}



